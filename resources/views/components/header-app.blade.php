<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
<header class="fixed z-10 w-full bg-white shadow-sm lg:shadow-none">
    <nav class="border-gray-200 py-2.5 rounded  ">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
            <a href="/" class="flex items-center text-lg font-semibold tracking-wide lg:text-2xl text-primary ">
                Naladhipa Course
            </a>
            <div class="flex md:order-2">
                @guest
                    <a href="/login"
                        class="px-5 py-2.5 mr-3 hover:shadow-lg font-semibold text-center text-white rounded-lg bg-primary hover:bg-purple-600 focus:ring-4 focus:outline-none focus:ring-purple-300 text-sm md:mr-0">Masuk</a>
                @endguest
                @auth
                <div class="flex gap-x-4">
                    <button type="button justify-center flex"
                        class="flex mr-3 text-sm rounded-full md:mr-0 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600"
                        id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown"
                        data-dropdown-placement="bottom">
                        <span class="sr-only">Open user menu</span>
                        @if (Auth::user()->role_id == 3)
                        <img class="w-10 h-10 border rounded-full border-1"
                        src="{{ asset('storage/' . Auth::user()->mentor[0]['photo']) }}" alt="image description">
                        @elseif(Auth::user()->role_id == 2)
                        <img class="w-10 h-10 border rounded-full border-1"
                        @if (Auth::user()->siswa[0]['photo'])
                        src="{{ asset('storage/siswa-photo/' . Auth::user()->siswa[0]['photo']) }}" alt="image description">
                        @endif
                        @else
                        <span class="p-1 svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo8/dist/../src/media/svg/icons/Communication/Shield-user.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3"/>
                                <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3"/>
                                <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                        @endif
                    </button>
                </div>
                    <!-- Dropdown menu -->
                    <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600"
                        id="user-dropdown">
                        <div class="px-4 py-3">
                            <span class="block text-sm text-gray-900 dark:text-white">{{ Auth::user()->name }}</span>
                            <span
                                class="block text-sm font-medium text-gray-500 truncate dark:text-gray-400">{{ Auth::user()->email }}</span>
                        </div>
                        <ul class="py-1" aria-labelledby="user-menu-button">
                            @if (Auth::user()->role_id != 2)
                                <li>
                                    <a href="{{ route('dashboard') }}"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Dashboard</a>
                                </li>
                            @endif
                            @if (Auth::user()->role_id == 2)     
                            <li>
                                <a href="{{route('profile')}}" class="flex w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Profile</a>
                            </li>
                            @endif
                            <li>
                                <form action="{{route('logout')}}" method="POST">
                                    @csrf
                                    <button 
                                        class="block w-full px-4 py-2 text-sm text-left text-gray-700 hover:bg-gray-100 ">Sign
                                        out</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                @endauth
                <button data-collapse-toggle="mobile-menu-2" type="button"
                    class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                    aria-controls="mobile-menu-2" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            @guest

                <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="mobile-menu-2">
                    <ul
                        class="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                        <li>
                            <a href="#"
                                class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white"
                                aria-current="page">Home</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">About</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">Services</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">Pricing</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">Contact</a>
                        </li>
                    </ul>
                </div>
            @endguest
            @auth
            @if (Auth::user()->role_id == 2)    
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="mobile-menu-2">
                <ul
                    class="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                    <li>
                        <a href="/"
                            class=" {{ request()->is('/') ? 'text-primary' : 'text-gray-700' }}block py-2 pl-3 pr-4 rounded md:bg-transparent hover:text-primary  md:p-0 "
                            aria-current="page">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('list.kelas') }}"
                            class=" {{ request()->is('kelas') ? 'text-primary' : 'text-gray-700' }} block py-2 pl-3 pr-4 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">Materi Pembejalaran</a>
                    </li>
                    <li>
                        <a href="{{route('list.exam')}}"
                            class="{{request()->is('exam') ?  'text-primary' : 'text-gray-700'}}block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-primary md:p-0 ">Quiz</a>
                    </li>
                </ul>
            </div>
            @endif
            @endauth
        </div>
        </div>
    </nav>
</header>
