@include('includes.dashboard.style')
<style>
    body {
        margin: 0;
        padding: 0;
        font-size: 9pt;
        font-family: 'poppins', 'sans-serif';
        -webkit-print-color-adjust: exact !important
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box
    }

    .my-float {
        margin-top: 22px;
        font-size: 18px !important
    }

    .page {
        width: 21cm;
        height: 29.7cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: #fff;
        box-shadow: 0 0 5px rgba(0, 0, 0, .1)
    }
</style>

<body>

    <div class=" book">
        <div class="page ">
            <div class="header">
                <div class="text-center gap-y-2">
                    <p class="text-3xl font-semibold tracking-wide">Naladhipa Course</p>
                    <p class="text-xl font-semibold tracking-wide">Bimbingan Belajar Bahasa Inggris</p>
                    <p class="text-sm tracking-wide">Alamat dan no telp</p>
                </div>
            </div>
            <div class="mx-auto " style="padding-left: 3rem !important; padding-right: 3rem !important;">
                <hr class="w-full py-2 ">
                <div class="w-6/12 px-4 py-2 text-white bg-purple-500 rounded-xl">
                    <p class="text-xl tracking-wide ">Laporan Hasil Belajar</p>
                </div>
                <div class="py-5 gap-y-2">
                    <p>Nama Peserta Didik&nbsp;&nbsp;&nbsp;: {{$rapor->siswa->user->name}} </p>
                    <p>Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$rapor->siswa->alamat}}
                    </p>
                    <p>Kelas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                            style="margin-left: 2.5px">: {{$rapor->siswa->kelas}}</span></p>
                    <p>Bulan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                            style="margin-left: 1.5px">: {{ Carbon\Carbon::parse($rapor->created_at)->format('F Y')}}</span></p>
                </div>
                <div>
                    <div class="py-5">
                        <table class="w-full border border-collapse border-slate-400">
                            <thead class="text-center bg-purple-100">
                                <tr>
                                    <th class="border border-slate-300">NO</th>
                                    <th class="px-8 border border-slate-300">Kompetensi</th>
                                    <th class="px-8 border border-slate-300">Nilai</th>
                                    <th class="px-8 border border-slate-300">Capaian Kompetensi</th>
                                </tr>
                            </thead>
                            <tbody class="text-sm">
                                <tr>
                                    <td class="text-center border border-slate-300">1</td>
                                    <td class="p-1 text-left border border-slate-300">Listening</td>
                                    <td class="text-center border border-slate-300">{{$rapor->listening ? $rapor->listening : ' '}}</td>
                                    <td class="p-1 text-left border border-slate-300">{{$rapor->listening_note ? $rapor->listening_note : ' '}}</td>

                                </tr>
                                <tr>
                                    <td class="text-center border border-slate-300">2</td>
                                    <td class="p-1 text-left border border-slate-300">Reading</td>
                                    <td class="text-center border border-slate-300">{{$rapor->reading ? $rapor->reading : ''}}</td>
                                    <td class="p-1 text-left border border-slate-300">{{$rapor->reading_note ? $rapor->reading_note : ''}}</td>

                                </tr>
                                <tr>
                                    <td class="text-center border border-slate-300">3</td>
                                    <td class="p-1 text-left border border-slate-300">Speaking</td>
                                    <td class="text-center border border-slate-300">{{$rapor->speaking ? $rapor->speaking : ''}}</td>
                                    <td class="p-1 text-left border border-slate-300">{{$rapor->speaking_note ? $rapor->speaking_note : ''}}</td>

                                </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="py-10">
                        <table class="w-full border border-collapse border-slate-400 ">
                            <thead class="text-center bg-purple-100">
                                <tr>

                                    <th class="border border-slate-300">Capaian Belajar Siswa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td class="p-1 text-sm text-left border border-slate-300">{{$rapor->progress_belajar}}</td>

                                </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="grid mt-5 justify-items-end">
                        <div class="text-lg">Surabaya, {{Carbon\Carbon::parse($rapor->created_at)->format('d-m-Y')}}</div>
                        <img class="w-[120px] h-[80px]" src="{{ asset('/assets/img/ttd.jpg') }}" alt="ttd">
                        <div class="text-lg">Albert</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="div-float">
        <a href="javascript:;" class="float" onclick="window.print();">
            <i class="fa fa-print my-float"></i>
        </a>
    </div>
</body>
@include('includes.dashboard.script')
