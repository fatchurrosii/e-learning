@extends('layouts.landing')
@section('content')
    <div class="mt-20"> 
        <h1 class="text-2xl font-semibold text-primary">Profile</h1>
        <div class="w-full h-full p-6 mx-auto mt-5 bg-white rounded shadow-sm gap-y-2">
            <div class="flex justify-between ">
                <div class="flex py-5 gap-x-6">
                    @if ($siswa->photo)    
                    <img class="w-20 h-20 border rounded-full border-1"
                        src="{{ asset('storage/siswa-photo/' . $siswa->photo)}}" alt="photo siswa">
                        @else
                        <img class="w-20 h-20 bg-purple-300 border rounded-full border-1"
                        src="{{ asset('assets/img/no-image.jpg')}}" alt="photo siswa">
                    @endif
                    <div class="self-center">
                        <div class="flex flex-nowrap gap-x-2">
                            <span class="svg-icon svg-icon-primary svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24"
                                    version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path
                                            d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                            fill="#8356FF" fill-rule="nonzero" opacity="0.3" />
                                        <path
                                            d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                            fill="#a788fc" fill-rule="nonzero" />
                                    </g>
                                </svg></span>
                            <h1 class="text-2xl font-medium text-dark">{{ Auth::user()->name }}</h1>
                        </div>
                        <div class="flex flex-nowrap gap-x-2">
                            <span class="svg-icon svg-icon-primary svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24"
                                    version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path
                                            d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                            fill="#a788fc" />
                                    </g>
                                </svg></span>
                            <h1 class="text-base text-gray-400">{{ Auth::user()->email }}</h1>
                        </div>
    
                    </div>
    
                </div>
                <div class="self-center col-span-3 mt-2 md:col-span-2">
                    <a href="{{ route('edit.profile') }}"
                        class="w-full p-3 text-white uppercase bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">Edit Profile</a>
                </div>
            </div>
            <hr class="w-full py-2">
            <h1 class="text-lg font-semibold text-primary">Biodata</h1>
            <div class="py-5">
                <div class="flex flex-col gap-y-2">
                    <p>Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $siswa->alamat }}</p>
                    <p>No Hp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $siswa->no_hp }}</p>
                    <p>kelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $siswa->kelas }}</p>
                </div>
            </div>
            <hr class="w-full py-2">
            <h1 class="text-lg font-semibold text-primary">History Exam</h1>
            <div class="relative py-5 mt-5 overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full text-sm text-left text-gray-500 ">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                        <tr class="items-center">
                            <th scope="col" class="px-6 py-3 ">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3 text-center">
                                Exam
                            </th>
                            <th scope="col" class="px-6 py-3 text-center">
                                Tanggal
                            </th>
                            <th scope="col" class="px-6 py-3 text-center">
                                Score
                        </tr>
                    </thead>
                   
                    <tbody>
                        @forelse ($test as $key => $item)
                        <tr
                        class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <td class="px-6 py-4">
                            <div class="pl-3">
                                <p class="text-base font-semibold">{{$key + 1}}</p>
                            </div>
                        </td>
                        <td class="px-6 py-4">
                            <p class="text-base font-medium text-center">{{$item->exam->name}}</p>
                        </td>
                        <td class="px-6 py-4">
                            <p class="text-base font-medium text-center">{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</p>
                        </td>
                        <td class="px-6 py-4">
                            <p class="text-base font-medium text-center">{{$item->score}}</p>
                        </td>
                    </tr>
                        @empty
                        <div class="p-3 mb-3 text-white bg-red-500 rounded shadow-sm">
                            Data Belum Tersedia!
                        </div>
                        @endforelse
                      
                    </tbody>
                </table>
                <div class="px-4 mt-2">
                    {{ $test->withQueryString()->links('vendor.pagination.tailwind') }}
                </div>
            </div>
            <hr class="w-full py-2 mt-5">
            <h1 class="text-lg font-semibold text-primary">Pengaturan</h1>
            <div class="flex py-5 gap-x-10">
                <div class="my-4">
                    <form onsubmit="return confirm('Password anda akan direset menjadi : 123456 ');" action="/reset-password" method="POST">
                        @csrf
                        <button type="submit"
                            class="px-4 py-1.5 bg-red-500/80 text-white rounded-lg"><span class="text-lg ">Reset Password</span></button>
                    </form>
                </div>
                <form  action="/logout" method="POST">
                    @csrf
                    <div class="my-4">
                        <button
                            class="px-4 py-1.5 bg-red-500/80 text-white rounded-lg"><span class="text-lg ">Logout</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
