@extends('layouts.landing')
@section('content')
<div class="flex justify-between mt-32">
    <span class="self-center text-gray-600">
     Edit Profile Siswa | <a href="{{URL::previous()}}">Profile </a> <span class="font-semibold"> - Edit Profle</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('profile') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="p-5 mt-5 bg-white rounded shadow-sm">
        <form action="{{ route('update.profile', $siswa->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                    Siswa
                </label>
                <input type="text" name="name" value="{{ old('name', $user->name) }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Nama" required>
                @error('no_hp')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="photo">
                    Photo Siswa</label>
                <input
                    class="block w-full mb-5 text-xs text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50"
                    id="photo" type="file" name="photo">
                    @if ($siswa->photo)
                        
                    <img width="120px" class="py-3 " src="{{asset('storage/siswa-photo/'. $siswa->photo)}}" alt="">
                    @endif
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Alamat</label>
                <textarea id="message" rows="4" name='alamat'
                    class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-gray-50 rounded-lg border border-gray-300"
                    placeholder="Alamat ..." required>{{ old('alamat', $siswa->alamat) }}</textarea>
                @error('alamat')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">No HP</label>
                <input type="text" name="no_hp" value="{{ old('no_hp', $siswa->no_hp) }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="No HP" required>
                @error('no_hp')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="kelas">
                    Kelas
                </label>
            </div>
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">UPDATE
                    PROFILE</button>
            </div>

        </form>
    </div>
@endsection
