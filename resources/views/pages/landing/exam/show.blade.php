@extends('layouts.app')

@section('content')
<div class="flex justify-between mt-28">
    <span class="self-center text-gray-600">
     List Exam | <a href="{{URL::previous()}}">Exam </a> 
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('user.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
   <div class="justify-between py-5 mt-10 bg-white">
    @php
    $counter = 1;
@endphp
@if (!empty($test_result))
<div class="px-6">
    <div class="w-full p-4 px-6 py-3 mb-4 text-sm font-medium text-blue-800 rounded-lg bg-blue-50">
        <span class="text-2xl">Score terakhir anda :  {{$test_result->score}}</span>
      </div>
</div>
@endif
 <div class="container mx-auto mt-5 gap-y-6">
    <form action="{{route('test', [$exam->id])}}" method="POST">
        @csrf
        @foreach ($question as $key => $item)
        @if ($item->quiz_images == !null)
        <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} .  {{$item->quiz_images->title}}</p>
        <img class="py-3 mx-auto" src="{{asset('storage/quiz-image/'. $item->quiz_images->images)}}" alt="gambar-soal">
        <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_images->caption}}</p>
         @foreach ($item->question_options as $option) 
         <div class="flex items-center mb-4">
             <input   id="default-radio-1" type="radio" value="{{$option->id}}" name="question[{{ $item->id }}]" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
             <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
         </div>
         @endforeach
         <p class="mb-5 text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         <hr class="py-2">
        @elseif($item->quiz_sounds == !null)
        <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_sounds->title}}</p>
        <div class="py-3 mx-auto">
               @if ($item->quiz_sounds->path)
                   <audio class="py-2 mx-auto" controls>
                       <source class="py-2" src="{{ asset('storage/' . $item->quiz_sounds->path) }}" type="audio/mpeg">
                   </audio>
               @endif
        </div>
        <p class="mb-3 text-lg font-normal leading-normal">{{$item->quiz_sounds->caption}}</p>
        @foreach ($item->question_options as $option) 
         <div class="flex items-center mb-4">
             <input   id="default-radio-1" type="radio" value="{{$option->id}}" name="question[{{ $item->id }}]" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
             <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
         </div>
         @endforeach
         <p class="mb-5 text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         <hr class="py-2">
        @elseif($item->quiz_videos == !null)
        <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_videos->title}} </p>
        <div class="py-3 mx-auto">
           <iframe class=" mx-auto rounded-md aspect-video lg:w-[800px] lg:h-[400px] w-[400px] h-[300px]"
              src="https://www.youtube.com/embed/{{ $item->quiz_videos->video }}" title="YouTube video player" frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen></iframe>
       </div>
       <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_videos->caption}}</p>
        @foreach ($item->question_options as $option) 
        <div class="flex items-center mb-4">
            <input   id="default-radio-1" type="radio" value="{{$option->id}}" name="question[{{ $item->id }}]" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
            <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium "> {{$option->option_text}}</label>
        </div>
        @endforeach
        <p class="mb-5 text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
        <hr class="py-2">
        @elseif($item->quiz_texts == !null)
        <p class="py-3 text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_texts->title}} </p>
        <div class="p-6 mx-auto border rounded-lg border-gray-50">
            <p class="font-normal leading-normal text-normal">{{$item->quiz_texts->text}}</p>
        </div>
        <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_texts->caption}}</p>
        @foreach ($item->question_options as $option) 
         <div class="flex items-center mb-4 ">
             <input   id="default-radio-1" type="radio" value="{{$option->id}}" name="question[{{ $item->id }}]" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
             <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium "> {{$option->option_text}}</label>
         </div>
         
         @endforeach
         <p class="mb-5 text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         <hr class="py-2">
        @endif 
        @php
        $counter++;
    @endphp
    @endforeach
    <div class="mt-2 mb-10 ">
       {{-- {{ $question->withQueryString()->links('vendor.pagination.tailwind') }} --}}
       <div>
           <button  class="px-6 py-3 text-white rounded-lg cursor-pointer bg-primary hover:bg-purple-700">Submit Jawaban</button>
       </div>
   </div>
    </form>
   
 </div>
 
   </div>
@endsection