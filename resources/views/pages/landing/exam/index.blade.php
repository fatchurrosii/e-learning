@extends('layouts.landing')

@section('content')
    <div class="mt-36">
    <h1 class="text-2xl">Halaman Quiz Naladhipa Course</h1>
                <div class="lg:grid lg:grid-cols-4 gap-x-16 gap-y-10">
                    @foreach ($quiz as $item)
                        <div class="py-2">
                            <div
                                class="max-w-sm p-6 bg-white border border-gray-200 shadow-md rounded-3xl dark:bg-gray-800 dark:border-gray-700">
                                <a href="{{ route('detail.exam', $item->id) }}">
                                    <img class="rounded-xl h-[300px] w-[400px]"
                                        src="{{ asset('storage/exam-image/' . $item->image) }}" alt="" />
                                </a>
                                <div class="p-2">
                                    <div class="flex justify-between">
                                        <p class="py-2 text-lg text-gray-900 dark:text-white">
                                            {{ $item->name }}</p>

                                    </div>
                                    <a href="{{ route('detail.exam', $item->id) }}"
                                        class="flex items-center w-full px-3 py-2 mt-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 ">
                                        Lihat Exam
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


    </div>
@endsection
