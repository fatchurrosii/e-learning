@extends('layouts.landing')

@section('content')
    <div class="mt-36">
        <h1 class="text-2xl">Halaman Materi Pembelajaran Naladhipa Course</h1>
        <div class="mb-4 border-b border-gray-200 dark:border-gray-700 ">
            <ul class="flex flex-wrap -mb-px text-sm font-medium text-center" id="myTab" data-tabs-toggle="#myTabContent"
                role="tablist">
                <li class="mr-2" role="presentation">
                    <button class="inline-block p-4 border-b-2 rounded-t-lg" id="profile-tab" data-tabs-target="#profile"
                        type="button" role="tab" aria-controls="profile" aria-selected="false">Speaking</button>
                </li>
                <li class="mr-2" role="presentation">
                    <button
                        class="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300"
                        id="dashboard-tab" data-tabs-target="#dashboard" type="button" role="tab"
                        aria-controls="dashboard" aria-selected="false">Writing</button>
                </li>
                <li class="mr-2" role="presentation">
                    <button
                        class="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300"
                        id="settings-tab" data-tabs-target="#settings" type="button" role="tab"
                        aria-controls="settings" aria-selected="false">Reading</button>
                </li>
            </ul>
        </div>
        <div id="myTabContent">
            <div class="hidden p-6 rounded-lg" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="lg:flex gap-x-16 gap-y-10">
                    @foreach ($speaking as $item)
                        <div class="py-2">
                            <div
                                class="max-w-sm p-6 bg-white border border-gray-200 shadow-md rounded-3xl dark:bg-gray-800 dark:border-gray-700">
                                <a href="{{ route('detail.kelas', $item->id) }}">
                                    <img class="rounded-xl h-[300px] w-[400px]"
                                        src="{{ asset('storage/kelas-photo/' . $item->image) }}" alt="" />
                                </a>
                                <div class="p-2">
                                    <h5 class="py-2 text-2xl font-bold tracking-wide text-gray-900 dark:text-white">
                                        {{ $item->title }}</h5>
                                    <div class="flex justify-between py-2">
                                        <span
                                            class="bg-blue-100 text-blue-800  text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full self-center">{{ $item->user->name }}</span>

                                        @if ($item->kelas_student->is_done == true)
                                            <div class="flex">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    class="icon icon-tabler icon-tabler-circle-check text-emerald-500"
                                                    width="32" height="32" viewBox="0 0 24 24" stroke-width="2"
                                                    stroke="currentColor" fill="none" stroke-linecap="round"
                                                    stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                    <circle cx="12" cy="12" r="9"></circle>
                                                    <path d="M9 12l2 2l4 -4"></path>
                                                </svg>
                                                <span class="self-center px-1 text-dark">Sudah dilihat</span>
                                            </div>
                                        @endif
                                    </div>
                                    {{-- {{$kelasVideo->id}} --}}
                                    <a href="{{ route('detail.kelas', $item->id) }}"
                                        class="flex items-center w-full px-3 py-2 mt-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 ">
                                        Lihat Kelas
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="hidden p-6 rounded-lg dark:bg-gray-800" id="dashboard" role="tabpanel"
                aria-labelledby="dashboard-tab">
                <div class="lg:flex gap-x-16 gap-y-10">
                    @foreach ($reading as $item)
                        <div class="py-2">
                            <div
                                class="max-w-sm p-6 bg-white border border-gray-200 shadow-md rounded-3xl dark:bg-gray-800 dark:border-gray-700">
                                <a href="{{ route('detail.kelas', $item->id) }}">
                                    <img class="rounded-xl h-[300px] w-[400px]"
                                        src="{{ asset('storage/kelas-photo/' . $item->image) }}" alt="" />
                                </a>
                                <div class="p-2">
                                    <h5 class="py-2 text-2xl font-bold tracking-wide text-gray-900 dark:text-white">
                                        {{ $item->title }}</h5>
                                    <div class="flex justify-between py-2">
                                        <span
                                            class="bg-blue-100 text-blue-800  text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full self-center">{{ $item->user->name }}</span>

                                        @if ($item->kelas_student->is_done == true)
                                            <div class="flex">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    class="icon icon-tabler icon-tabler-circle-check text-emerald-500"
                                                    width="32" height="32" viewBox="0 0 24 24" stroke-width="2"
                                                    stroke="currentColor" fill="none" stroke-linecap="round"
                                                    stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                    <circle cx="12" cy="12" r="9"></circle>
                                                    <path d="M9 12l2 2l4 -4"></path>
                                                </svg>
                                                <span class="self-center px-1 text-dark">Sudah dilihat</span>
                                            </div>
                                        @endif
                                    </div>
                                    <a href="{{ route('detail.kelas', $item->id) }}"
                                        class="flex items-center w-full px-3 py-2 mt-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 ">
                                        Lihat Kelas
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="hidden p-6 rounded-lg dark:bg-gray-800" id="settings" role="tabpanel"
                aria-labelledby="settings-tab">
                <div class="lg:flex gap-x-16 gap-y-10">
                    @foreach ($writing as $item)
                        <div class="py-2">
                            <div
                                class="max-w-sm p-6 bg-white border border-gray-200 shadow-md rounded-3xl dark:bg-gray-800 dark:border-gray-700">
                                <a href="{{ route('detail.kelas', $item->id) }}">
                                    <img class="rounded-xl h-[300px] w-[400px]"
                                        src="{{ asset('storage/kelas-photo/' . $item->image) }}" alt="" />
                                </a>
                                <div class="p-2">
                                    <h5 class="py-2 text-2xl font-bold tracking-wide text-gray-900 dark:text-white">
                                        {{ $item->title }}</h5>
                                    <div class="flex justify-between py-2">
                                        <span
                                            class="bg-blue-100 text-blue-800  text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full self-center">{{ $item->user->name }}</span>

                                        @if ($item->kelas_student->is_done == true)
                                            <div class="flex">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    class="icon icon-tabler icon-tabler-circle-check text-emerald-500"
                                                    width="32" height="32" viewBox="0 0 24 24" stroke-width="2"
                                                    stroke="currentColor" fill="none" stroke-linecap="round"
                                                    stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                    <circle cx="12" cy="12" r="9"></circle>
                                                    <path d="M9 12l2 2l4 -4"></path>
                                                </svg>
                                                <span class="self-center px-1 text-dark">Sudah dilihat</span>
                                            </div>
                                        @endif
                                    </div>
                                    <a href="{{ route('detail.kelas', $item->id) }}"
                                        class="flex items-center w-full px-3 py-2 mt-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 ">
                                        Lihat Kelas
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>


    </div>
@endsection
