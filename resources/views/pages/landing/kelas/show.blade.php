@extends('layouts.app')

@section('content')
    <div class="mt-36 lg:mx-auto h-screen">
        {{-- <iframe class="w-full aspect-video ..." src="https://www.youtube.com/embed/{{ $kelasVideo->embed_video }}"></iframe> --}}
        <iframe class="rounded-md aspect-video lg:w-[900px] lg:h-[550px] w-[400px] h-[300px]"
            src="https://www.youtube.com/embed/{{ $kelasVideo->embed_video }}" title="YouTube video player" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen></iframe>
        <h1 class="text-2xl lg:text-4xl font-bold py-2 lg:py-5 tracking-wide ">{{ $kelasVideo->title }}</h1>
        <p class="text-base lg:text-xl font-medium leading-relaxed text-dark">{{ $kelasVideo->deskripsi }}</p>
        <form action="{{ route('changeStatus', $kelasVideo->id) }}" method="POST">
            @csrf
            @method('PUT')
            <button type="sumbit"
                class="w-full text-white font-bold bg-primary py-2 rounded-md tracking-wider uppercase">Selesai</button>
        </form>
    </div>
@endsection
