@extends('layouts.dashboard')

@section('title', 'User')

@section('content')

    <div class="p-5 mt-20 bg-white rounded shadow-sm">
        <div class="flex justify-between py-2 mb-3">
            <div>
                <p class="text-base font-semibold ">
                    Data User
                </p>
                <p class="text-sm text-slate-500">Daftar user <span class="text-primary"> Naladhipa Courses</span></p>
            </div>
            <div class="col-span-3 mt-2 md:col-span-2">
                <a href="{{ route('user.create') }}"
                    class="w-full p-3 text-white uppercase bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">TAMBAH
                    user</a>
            </div>
        </div>
        <div class="grid grid-cols-6 mb-4 md:grid-cols-8 md:gap-4 ">
            <div class="col-span-3 md:col-span-3">
                <form action="{{ route('user.index') }}" method="GET">
                    <input type="text" name="keyword"
                        class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Search...">
                </form>
            </div>
        </div>

        <div class="overflow-x-auto md:relative sm:rounded-lg">
            <div class="flex items-center justify-between pb-4">

            </div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
                    <tr class="text-center">
                        <th scope="col" class="px-6 py-3 ">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Nama
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Email
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Role
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Status
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Aksi
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $key => $user)
                        <tr class="text-center bg-white border-b hover:bg-gray-50">
                            <td scope="row" class="px-6 py-4 text-gray-900 ">
                                <div class="pl-3">
                                    <p class="text-base font-semibold">{{$key + $users->firstItem()}}</p>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $user->name }}</p>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $user->email }}</p>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $user->role->name }}</p>
                            </td>
                            <td class="px-6 py-4 text-center">
                                
                                @if ($user->status == 'Active')      
                                <a class="px-3 py-2 rounded-md bg-emerald-500 " href="{{route('change-status',['id' => $user->id, 'status' => 'Inactive'])}}">
                                    <i class="fa-solid fa-check" style="color: #ffffff;"></i>
                                </a>
                                @else
                                
                                <a href="{{route('change-status',['id' => $user->id, 'status' => 'Active'])}}" class="px-3 py-2 bg-red-500 rounded-md " >
                                    <i class="fa-solid fa-ban" style="color: #ffffff;"></i>
                                    </a>
                                @endif
                                   </td>
                            <td class="inline-flex items-center self-center px-6 py-4 ">
                                <button id="dropdownDefaultButton " data-dropdown-toggle="dropdown-{{ $user }}"
                                    class="self-center text-white bg-gray-400 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center"
                                    type="button">Aksi <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none"
                                        stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 9l-7 7-7-7"></path>
                                    </svg></button>
                                <!-- Dropdown menu -->
                                <div id="dropdown-{{ $user }}"
                                    class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                                    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200"
                                        aria-labelledby="dropdownDefaultButton">
                                        <li>
                                            <a href="{{ route('user.edit', $user->id) }}"
                                                class="block px-4 py-2 text-left ">Edit</a>
                                        </li>
                                        <li>
                                            <form onsubmit="return confirm('Password anda akan direset menjadi : 123456 ');" action="{{route('reset', $user->id)}}" method="POST">
                                                @csrf
                                                <button type="submit"
                                                    class="block px-4 py-2 text-left">Reset Password</button>
                                            </form>
                                        </li>
                                        <li>
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                action="{{ route('user.destroy', $user->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="w-full px-4 py-2 text-left delete-confirm hover:bg-gray-100 focus:outline-none">
                                                    Delete</button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="p-3 mb-3 text-white bg-red-500 rounded shadow-sm">
                            Data Belum Tersedia!
                        </div>
                    @endforelse
                </tbody>
            </table>
            <div class="mt-2">
                {{ $users->links('vendor.pagination.tailwind') }}
            </div>
        </div>
    @endsection
