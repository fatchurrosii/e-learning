@extends('layouts.dashboard')

@section('title', 'Tambah User')

@section('content')
    <div class="flex justify-between mt-10">
       <span class="self-center text-gray-600">
        Tambah Data User | <a href="{{URL::previous()}}">Data User </a> <span class="font-semibold"> - Tambah User</span>
    </span> 
    <div class="col-span-3 mt-2 md:col-span-2">
        <a href="{{ route('user.index') }}"
            class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
    </div>
    </div>
    <div class="p-5 mt-5 bg-white rounded shadow-sm">
        <form action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Nama User</label>
                <input type="text" name="name" value="{{ old('name') }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Nama User" required>
                @error('name')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Email User</label>
                <input type="email" name="email" value="{{ old('email') }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Email User" required>
                @error('email')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Password User</label>
                <input type="text" name="password" value="{{ old('password') }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Password" required>
                @error('password')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                    Role
                </label>
                <div class="relative">
                    <select
                        class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                        id="role" name="role_id">
                        @foreach ($roles as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="status">
                    Status
                </label>
                <div class="relative">
                    <select
                        class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                        id="status" name="status">
                        <option value="Inactive">Inactive</option>
                        <option value="Active">Active</option>
                    </select>
                </div>
            </div>
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                    USER</button>
            </div>

        </form>
    </div>
@endsection
