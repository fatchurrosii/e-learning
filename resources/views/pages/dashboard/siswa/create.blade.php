@extends('layouts.dashboard')

@section('title', 'Tambah Siswa')

@section('content')

<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Tambah Data Siswa | <a href="{{URL::previous()}}">Data Siswa </a> <span class="font-semibold"> - Tambah Siswa</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('siswa.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="min-h-screen">
        <div class="p-5 pb-20 mt-5 bg-white rounded shadow-sm ">
            <form action="{{ route('siswa.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Siswa
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="user" name="user_id" required>
                            @foreach ($user as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Mentor
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            name="mentor_id" required>
                            <option>Select an item</option>
                            @foreach ($mentor as $item)
                                <option value="{{ $item->id }}"
                                    {{ old('mentor_id') == $item ? 'selected' : '' }}>{{ $item->user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="photo">
                        Photo Siswa</label>
                    <input
                        class="block w-full mb-5 text-xs text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                        id="photo" type="file" name="photo">
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Alamat</label>
                    <textarea id="message" rows="4" name='alamat' value="{{ old('alamat') }}"
                        class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-gray-50 rounded-lg border border-gray-300"
                        placeholder="Alamat ..." required>{{ old('alamat') }}</textarea>
                    @error('alamat')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">No HP</label>
                    <input type="text" name="no_hp" value="{{ old('no_hp') }}"
                        class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                        placeholder="No HP" required>
                    @error('no_hp')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="kelas">
                        Kelas
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="kelas" name="kelas">
                            <option value="Online" {{ old('kelas') == 'Online' ? 'selected' : '' }}>Online</option>
                            <option value="Offline"{{ old('kelas') == 'Offline' ? 'selected' : '' }}>Offline</option>
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <button type="submit"
                        class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                        SISWA</button>
                </div>
            </form>
        </div>
    </div>
@endsection
