@extends('layouts.dashboard')

@section('title', 'Kategori Siswa')

@section('content')
    <div class="p-5 mt-20 bg-white rounded shadow-sm">
        <div class="flex justify-between py-2 mb-3">
            <div >
                <p class="text-base font-semibold ">
                    Data Siswa
                </p>
                <p class="text-sm text-slate-500">Daftar siswa <span class="text-primary"> Naladhipa Courses</span></p>
            </div>
            <div class="col-span-3 mt-2 md:col-span-2">
                <a href="{{ route('siswa.create') }}"
                    class="w-full p-3 text-white uppercase bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">TAMBAH
                    Siswa</a>
            </div>
        </div>
        <div class="grid grid-cols-6 mb-4 md:grid-cols-8 md:gap-4 ">
            <div class="col-span-3 md:col-span-3">
                <form action="{{ route('siswa.index') }}" method="GET">
                    <input type="text" name="keyword"
                        class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Search...">
                </form>
            </div>
        </div>

        <div class=" md:relative sm:rounded-lg">
            <div class="flex items-center justify-between pb-4">

            </div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr class="items-center">
                        <th scope="col" class="px-6 py-3 ">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3 ">
                            Name
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Alamat
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Kelas
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Status
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Aksi
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @forelse ($siswa as $key => $item)
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <td class="px-6 py-4">
                                <div class="pl-3">
                                    <p class="text-base font-semibold">{{ $key + 1 }}</p>
                                </div>
                            </td>
                            <td
                                scope="row "class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
                                <img class="w-10 h-10 rounded-full" src="{{ asset('storage/siswa-photo/' . $item->photo ) }}"
                                    alt="Siswa Image">
                                <div class="pl-3">
                                    <div class="text-base font-semibold">{{ $item->user->name ?? '-' }}</div>
                                    <div class="font-normal text-gray-500">{{ $item->no_hp ?? '' }}</div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $item->alamat ?? '-' }}</p>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $item->kelas ?? '-' }}</p>
                            </td>
                            <td class="px-6 py-4 text-center">
                                @if ($item->user->status == 'Active')
                                    <p class="p-1 text-base font-medium rounded-lg text-emerald-600 bg-emerald-50">
                                        {{ $item->user->status }}</p>
                                @else
                                    <p class="p-1 text-base font-medium text-red-600 rounded-lg bg-red-50">
                                        {{ $item->user->status }}
                                    </p>
                                @endif
                            </td>
                            <td class="inline-flex items-center self-center px-6 py-4 ">
                                <button id="dropdownDefaultButton " data-dropdown-toggle="dropdown-{{$item}}"
                                    class="self-center text-white bg-gray-400 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center"
                                    type="button">Aksi <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none"
                                        stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 9l-7 7-7-7"></path>
                                    </svg></button>
                                <!-- Dropdown menu -->
                                <div id="dropdown-{{$item}}"
                                    class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                                    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200"
                                        aria-labelledby="dropdownDefaultButton">
                                        <li>
                                            <a href="{{ route('siswa.edit', $item->id) }}" class="block px-4 py-2 ">Edit</a>
                                        </li>
                                        <li>
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                action="{{ route('siswa.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="w-full px-4 py-2 text-left delete-confirm hover:bg-gray-100 focus:outline-none">
                                                    Delete</button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="p-3 mb-3 text-white bg-red-500 rounded shadow-sm">
                            Data Belum Tersedia!
                        </div>
                    @endforelse

                </tbody>
            </table>
            <div class="mt-2">
                {{ $siswa->withQueryString()->links('vendor.pagination.tailwind') }}
            </div>
        </div>
    </div>
@endsection
