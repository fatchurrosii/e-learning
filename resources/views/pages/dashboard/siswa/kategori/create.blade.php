@extends('layouts.dashboard', ['title' => 'Tambah Kategori'])

@section('content')
    <div class="p-5 bg-white rounded shadow-sm mt-36">
        <form action="{{ route('kategoriSiswa.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">NAMA KATEGORI
                    SISWA</label>
                <input type="text" name="name" value="{{ old('name') }}"
                    class="w-full p-2 mt-2 bg-gray-200 border border-gray-200 rounded shadow-sm focus:outline-none focus:bg-white"
                    placeholder="Nama Kategori Siswa">

            </div>
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                    KATEGORI SISWA</button>
            </div>

        </form>
    </div>
@endsection
