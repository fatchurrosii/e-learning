@extends('layouts.dashboard')

@section('title', 'Tambah Question')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Tambah Data Exam | <a href="{{URL::previous()}}">Data Exam </a> <span class="font-semibold"> - Tambah Exam</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('exam.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="p-5 mt-5 mb-20 bg-white rounded shadow-sm">
        <form action="{{ route('exam.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Title</label>
                <input type="text" name="title" value="{{ old('title') }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Cth: Soal Bahasa Inggris" required>
                @error('name')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="image">
                    Image</label>
                <input
                    class="block w-full mb-5 text-xs text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-zinc-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                    id="image" type="file" name="image">
            </div>
            <div class="field_wrapper">
                <div class="relative">
                  <div class="flex gap-x-6">
                    <select name="questions[]" id="" class="block w-full px-4 py-3 pr-8 mb-3 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500">
                        <option value="">Select An Option</option>
                       @foreach ($question as $item)
                       <option value="{{$item->id}}">{{$item->name}}</option>
                       @endforeach 
                    </select>
                      <a href="javascript:void(0);" class="px-6 mt-2 add_button " title="Add field"><img src="{{ asset('/assets/img/plus-circle.svg') }}"></a>
                  </div>
                </div>
            </div>
            
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                    EXAM</button>
            </div>

        </form>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class= "flex gap-x-6"><select name="questions[]" id="" class="block w-full px-4 py-3 pr-8 mb-3 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"><option value="">Select An Option</option>@foreach($question as $item)<option value="{{$item->id}}">{{$item->name}}</option>@endforeach </select><a href="javascript:void(0);" class="px-6 mt-2 remove_button"><img src={{asset('assets/img/minus-circle.svg')}}></a></div>'; //New input field html 
            var x = 1; //Initial field counter is 1
            
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){ 
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });
            
            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
        </script>
@endsection

     