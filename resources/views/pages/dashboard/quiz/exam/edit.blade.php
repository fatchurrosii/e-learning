@extends('layouts.dashboard')

@section('title', 'Tambah Question')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Edit Data Exam | <a href="{{URL::previous()}}">Data Exam </a> <span class="font-semibold"> - Edit Exam</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('exam.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="p-5 mt-5 mb-20 bg-white rounded shadow-sm">
        <form action="{{ route('exam.update', $exam->id) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Title</label>
                <input type="text" name="title" value="{{ old('title', $exam->name) }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Cth: Soal Bahasa Inggris" required>
                @error('name')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="image">
                    Image</label>
                <input
                    class="block w-full mb-5 text-xs text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-zinc-50 "
                    id="image" type="file" name="image">
                    @if ($exam->image)
                    <img class="py-5 w-[400px] h-[400px]" src="{{asset('storage/exam-image/' . $exam->image)}}">
                @endif
            </div>
            <div class="field_wrapper">
                <div class="relative">
                    @foreach($exam->exam_question as $item)
                    <div class="flex gap-x-6">
                        <select name="questions[]" id="" class="block w-full px-4 py-3 pr-8 mb-3 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500">
                        {{-- {{$item}} --}}
                        {{-- <option value="">Select An Option</option> --}}
                        @foreach($questions as $question)
                       {{-- <option value="{{$question->id}}">{{old($item->question->id) == $question->id ? 'selected' : ''}} {{$question->name}}</option> --}}
                       <option value="{{$item->question->id}}" {{$item->question->id == $question->id ? 'selected' : ''}}>{{old($item->question->name,$question->name)}}</option>
                       @endforeach
                    </select>
                </div>
                @endforeach
                </div>
            </div>
            
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                    EXAM</button>
            </div>

        </form>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

     