@extends('layouts.dashboard')

@section('title', 'Tambah Question')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Detail Exam | <a href="{{URL::previous()}}">Data Exam </a> <span class="font-semibold"> - Detail Exam</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('exam.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
   <div class="justify-between py-5 mt-10 bg-white ">
    @php
    $counter = 1;
@endphp
 <div class="container mx-auto mt-5 gap-y-6">   
     @foreach ($question as $key => $item)
         @if ($item->quiz_images == !null)
         <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} .  {{$item->quiz_images->title}}</p>
         <img class="py-3 mx-auto" src="{{asset('storage/quiz-image/'. $item->quiz_images->images)}}" alt="gambar-soal">
         <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_images->caption}}</p>
          @foreach ($item->question_options as $option) 
          <div class="flex items-center mb-4">
              <input  {{$option->correct  ?  'checked' : ' '}} id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
              <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
          </div>
          @endforeach
          <p class="text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         @elseif($item->quiz_sounds == !null)
         <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_sounds->title}}</p>
         <div class="py-3 mx-auto">
             <label class="block py-2 mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase " for="path">
                Audio</label>
                @if ($item->quiz_sounds->path)
                    <audio class="py-2" controls>
                        <source class="py-2" src="{{ asset('storage/' . $item->quiz_sounds->path) }}" type="audio/mpeg">
                    </audio>
                @endif
         </div>
         <p class="text-lg font-normal leading-normal">{{$item->quiz_sounds->caption}}</p>
         @foreach ($item->question_options as $option) 
          <div class="flex items-center mb-4">
              <input  {{$option->correct  ?  'checked' : ' '}} id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
              <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
          </div>
          @endforeach
          <p class="text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         @elseif($item->quiz_videos == !null)
         <p class="text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_videos->title}} </p>
         <div class="py-3 mx-auto">
            <iframe class="rounded-md aspect-video lg:w-[800px] lg:h-[400px] w-[400px] h-[300px]"
               src="https://www.youtube.com/embed/{{ $item->quiz_videos->video }}" title="YouTube video player" frameborder="0"
               allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
               allowfullscreen></iframe>
        </div>
        <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_videos->caption}}</p>
         @foreach ($item->question_options as $option) 
         <div class="flex items-center mb-4">
             <input  {{$option->correct  ?  'checked' : ' '}} id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
             <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
         </div>
         @endforeach
         <p class="text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         @elseif($item->quiz_texts == !null)
         <p class="py-3 text-xl font-semibold leading-relaxed">{{$key + $question->firstItem()}} . {{$item->quiz_texts->title}} </p>
         <div class="p-6 border rounded-lg border-gray-50">
             <p class="font-normal leading-normal text-normal">{{$item->quiz_texts->text}}</p>
         </div>
         <p class="py-3 text-lg font-normal leading-normal">{{$item->quiz_texts->caption}}</p>
         @foreach ($item->question_options as $option) 
          <div class="flex items-center mb-4">
              <input  {{$option->correct  ?  'checked' : ' '}} id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300">
              <label for="default-radio-{{ $loop->index }}" class="ml-2 text-sm font-medium"> {{$option->option_text}}</label>
          </div>
          
          @endforeach
          <p class="text-lg font-normal leading-normal"> Score : {{$item->score}}</p>
         @endif 
         @php
         $counter++;
     @endphp
     @endforeach
     <div class="mt-2 ">
        {{ $question->withQueryString()->links('vendor.pagination.tailwind') }}
    </div>
 </div>
 
   </div>
@endsection

     