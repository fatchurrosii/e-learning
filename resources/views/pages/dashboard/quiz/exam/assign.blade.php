@extends('layouts.dashboard')

@section('title', 'Tambah Question')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Assign Exam Siswa | <a href="{{URL::previous()}}">Data Exam</a> <span class="font-semibold"> - Tambah Assign Exam</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('exam.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="p-5 mt-5 mb-20 bg-white rounded shadow-sm">
        <form action="{{ route('store_assign', $exam->id) }}" method="POST" >
            @method('POST')
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Kelas</label>
                <div class="relative">
                    <select
                        class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                        name="exam_id" required disabled>
                            <option value="{{ $exam->id }}"
                                {{ $exam->id == $exam->id ? 'selected' : ' ' }}>
                                {{ $exam->name }}</option>
                    </select>
                </div>
            </div>
            <div class="mt-5 field_wrapper">
                <div class="relative">
                  <div class="flex gap-x-6">
                    <select name="siswa_ids[]" id="" class="block w-full px-4 py-3 pr-8 mb-3 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500">
                        <option value="">Select An Option</option>
                       @foreach ($siswa as $item)
                       {{$item->user->name}}
                       <option value="{{$item->id}}">{{$item->user->name}}</option>
                       @endforeach 
                    </select>
                      <a href="javascript:void(0);" class="px-6 mt-2 add_button " title="Add field"><img src="{{ asset('/assets/img/plus-circle.svg') }}"></a>
                  </div>
                </div>
            </div>
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">ASSIGN EXAM</button>
            </div>

        </form>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class= "flex gap-x-6"><select name="siswa_ids[]" id="" class="block w-full px-4 py-3 pr-8 mb-3 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"><option value="">Select An Option</option>@foreach($siswa as $item)<option value="{{$item->id}}">{{$item->user->name}}</option>@endforeach </select><a href="javascript:void(0);" class="px-6 mt-2 remove_button"><img src={{asset('assets/img/minus-circle.svg')}}></a></div>'; //New input field html 
            var x = 1; //Initial field counter is 1
            
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){ 
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });
            
            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
        </script>
@endsection

     