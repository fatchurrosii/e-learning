@extends('layouts.dashboard')

@section('title', 'Image')

@section('content')
    <div class="p-5 mt-20 bg-white rounded shadow-sm">
        <div class="flex justify-between py-2 mb-3">
            <div >
                <p class="text-base font-semibold ">
                    Master Data
                </p>
                <p class="text-sm text-slate-500"> <span class="text-primary"> Naladhipa Courses</span></p>
            </div>
            <div class="col-span-3 mt-2 md:col-span-2">
                <a href="javascript;"
                    class="w-full p-3 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700 modal-open ">Tambah
                    Data</a>
            </div>
        </div>
        <div class="grid grid-cols-6 mb-4 md:grid-cols-8 md:gap-4 ">
            <div class="col-span-3 md:col-span-3">
                <form action="{{ route('quiz-text.index') }}" method="GET">
                    <input type="text" name="keyword"
                        class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Search...">
                </form>
            </div>
        </div>

        <div class="md:relative sm:rounded-lg">
            <div class="flex items-center justify-between pb-4">

            </div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr class="items-center">
                        <th scope="col" class="px-6 py-3 ">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3 ">
                            Title
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Aksi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($text as $key => $item)
                        <tr class="bg-white border-b hover:bg-gray-50 ">
                            <td class="px-6 py-4">
                                <div class="pl-3">
                                    <p class="text-base font-semibold">{{ $key + 1 }}</p>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $item->title }}</p>
                            </td>
                            <td class="inline-flex items-center self-center px-6 py-4 ">
                                <button id="dropdownDefaultButton" data-dropdown-toggle="dropdown-{{ $item }}"
                                    class="self-center text-white bg-gray-400 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center"
                                    type="button">Aksi <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none"
                                        stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 9l-7 7-7-7"></path>
                                    </svg></button>
                                <!-- Dropdown menu -->
                                <div id="dropdown-{{ $item }}"
                                    class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                                    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200"
                                        aria-labelledby="dropdownDefaultButton">
                                        <li>
                                            <a href="{{ route('quiz-text.edit', $item->id) }}"
                                                class="block px-4 py-2 ">Edit</a>
                                        </li>
                                        <li>
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                action="{{ route('quiz-text.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="w-full px-4 py-2 text-left hover:bg-gray-100 focus:outline-none">
                                                    Delete</button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="p-3 mb-3 text-white bg-red-500 rounded shadow-sm">
                            Data Belum Tersedia!
                        </div>
                    @endforelse

                </tbody>
            </table>
            <div class="mt-2">
                {{ $text->withQueryString()->links('vendor.pagination.tailwind') }}
            </div>
        </div>
    </div>


    <!--Modal-->
    <div class="fixed top-0 left-0 flex items-center justify-center w-full h-full opacity-0 pointer-events-none modal">
        <div class="absolute w-full h-full bg-gray-900 opacity-50 modal-overlay"></div>

        <div class="z-50 w-11/12 mx-auto overflow-y-auto bg-white rounded shadow-lg modal-container md:max-w-md">

            <!-- Add margin if you want to see some of the overlay behind the modal-->
            <div class="px-6 py-4 text-left modal-content">
                <!--Title-->
                <div class="flex items-center justify-between pb-3">
                    <p class="text-2xl font-bold">Tambah Data</p>
                    <div class="z-50 cursor-pointer modal-close">
                        <svg class="text-black fill-current" xmlns="http://www.w3.org/2000/svg" width="18"
                            height="18" viewBox="0 0 18 18">
                            <path
                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                            </path>
                        </svg>
                    </div>
                </div>
                <form action="{{ route('quiz-text.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <!--Body-->
                    <div class="mt-5">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Judul
                            Pertanyaan</label>
                        <input type="text" name="title" value="{{ old('title') }}"
                            class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                            placeholder="Cth: Soal gambar" required>
                        @error('title')
                            <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mt-5">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">text
                            Soal</label>
                        <textarea id="message" rows="4" name='text' value="{{ old('text') }}"
                            class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-zinc-50 rounded-lg border border-gray-300"
                            placeholder="Deskripsi kelas ..." required>{{ old('text') }}</textarea>
                        @error('caption')
                            <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mt-5">
                        <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Caption
                            Soal</label>
                        <textarea id="message" rows="4" name='caption' value="{{ old('caption') }}"
                            class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-zinc-50 rounded-lg border border-gray-300"
                            placeholder="Deskripsi kelas ..." required>{{ old('caption') }}</textarea>
                        @error('caption')
                            <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <!--Footer-->
                    <div class="flex justify-end pt-2">
                        <button type="submit"
                            class="p-3 px-4 mr-2 text-white bg-indigo-500 rounded-lg hover:bg-indigo-700">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    <script>
        var openmodal = document.querySelectorAll('.modal-open')
        for (var i = 0; i < openmodal.length; i++) {
            openmodal[i].addEventListener('click', function(event) {
                event.preventDefault()
                toggleModal()
            })
        }

        const overlay = document.querySelector('.modal-overlay')
        overlay.addEventListener('click', toggleModal)

        var closemodal = document.querySelectorAll('.modal-close')
        for (var i = 0; i < closemodal.length; i++) {
            closemodal[i].addEventListener('click', toggleModal)
        }

        document.onkeydown = function(evt) {
            evt = evt || window.event
            var isEscape = false
            if ("key" in evt) {
                isEscape = (evt.key === "Escape" || evt.key === "Esc")
            } else {
                isEscape = (evt.keyCode === 27)
            }
            if (isEscape && document.body.classList.contains('modal-active')) {
                toggleModal()
            }
        };


        function toggleModal() {
            const body = document.querySelector('body')
            const modal = document.querySelector('.modal')
            modal.classList.toggle('opacity-0')
            modal.classList.toggle('pointer-events-none')
            body.classList.toggle('modal-active')
        }
    </script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/datepicker.min.js"></script>
@endsection
