@extends('layouts.dashboard')

@section('title', 'Edit Quiz Image')

@section('content')
<div class="flex justify-between mt-20">
    <span class="self-center text-gray-600">
     Edit Data  | <a href="{{ route('quiz-video.index') }}">Master Data  </a> <span class="font-semibold"> - Edit Data</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('quiz-video.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="min-h-screen pb-10 mt-10">
        <div class="p-5 bg-white rounded shadow-sm ">
            <form action="{{ route('quiz-video.update', $video->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Judul
                        Pertanyaan</label>
                    <input type="text" name="title" value="{{ old('title', $video->title) }}"
                        class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Cth: Soal gambar" required>
                    @error('title')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Video
                        Pertanyaan</label>
                    <div class="flex">
                        <span class="inline-flex items-center p-2 px-3 text-sm text-gray-900 bg-gray-100 border border-r-0 border-gray-300 rounded-l-md ">
                            Youtube.com/
                          </span>
                          <input type="text" name="video" value="{{ old('video', $video->video) }}"
                              class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                              placeholder="....." required>
                    </div>
                    @error('title')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Caption
                        Soal</label>
                    <textarea id="message" rows="4" name='caption' value="{{ old('caption', $video->caption) }}"
                        class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-zinc-50 rounded-lg border border-gray-300"
                        placeholder="Deskripsi soal ..." required>{{ old('caption', $video->caption) }}</textarea>
                    @error('caption')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <button type="submit"
                        class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                        QUIZ</button>
                </div>
            </form>
        </div>
    </div>
@endsection
