@extends('layouts.dashboard')

@section('title', 'Tambah Question')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Edit Data Question  | <a href="{{URL::previous()}}">Master Data Question </a> <span class="font-semibold"> - Edit Data Question</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('question.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="p-5 mt-10 mb-20 bg-white rounded shadow-sm">
        <form action="{{ route('question.update', $question->id) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="mt-5">
                <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Title</label>
                <input type="text" name="name" value="{{ old('name', $question->name) }}"
                    class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                    placeholder="Cth: Soal Bahasa Inggris" required>
                @error('name')
                    <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="justify-between lg:flex">
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Quiz Image
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="role" name="images_id">
                            <option value="">Select an option</option>
                            @foreach ($quizImage as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == old('images_id', $question->quiz_images_id) ? 'selected' : '' }}>
                                    {{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Quiz Audio
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="role" name="sounds_id">
                            <option value="">Select an option</option>
                            @foreach ($quizSound as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == old('sounds_id', $question->quiz_sounds_id) ? 'selected' : '' }}>
                                    {{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Quiz Text
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="role" name="text_id">
                            <option value="">Select an option</option>
                            @foreach ($quizText as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == old('text_id', $question->quiz_text_id) ? 'selected' : '' }}>
                                    {{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Quiz Video
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="role" name="videos_id">
                            <option value="">Select an option</option>
                            @foreach ($quizVideo as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == old('videos_id', $question->quiz_videos_id) ? 'selected' : '' }}>
                                    {{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @for ($i = 1; $i <= 4; $i++)
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase">Option Text</label>
                    <input type="text" name="{{ 'option_text' . $i }}"
                        value="{{ old('option_text' . $i, optional($question->question_options)[$i - 1]->option_text ?? '') }}"
                        class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                        placeholder="Cth: Sebutkan ..." required>
                    @error('option_text' . $i)
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="block py-2 text-xs font-bold tracking-wide text-gray-700 uppercase">
                    <label for="{{ 'correct' . $i }}">Correct*</label>
                    <input type="checkbox" name="{{ 'correct' . $i }}" value="1" id="{{ 'correct' . $i }}"
                        @if (old('correct' . $i, optional($question->question_options)[$i - 1]->correct)) checked @endif>
                    <p class="help-block"></p>
                    @error('correct' . $i)
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            @endfor
            <label class="block mt-5 mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Score</label>
            <input type="number" name="score" value="{{ old('score', $question->score) }}"
                class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-slate-50 focus:outline-none focus:bg-white"
                placeholder="100" required>
            @error('score')
                <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                    {{ $message }}
                </div>
            @enderror
            <div class="mt-5">
                <button type="submit"
                    class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                    QUESTION</button>
            </div>
    </div>

    </form>
    </div>
@endsection
