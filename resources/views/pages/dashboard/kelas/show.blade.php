@extends('layouts.dashboard')

@section('title', 'Kelas')

@section('content')
    <div class="mt-10">
        <a href="{{ URL::previous() }}">
            <div class="py-3 px-6 rounded-md">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler bg-white rounded-full icon-tabler-arrow-left"
                    width="48" height="48" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <line x1="5" y1="12" x2="19" y2="12"></line>
                    <line x1="5" y1="12" x2="11" y2="18"></line>
                    <line x1="5" y1="12" x2="11" y2="6"></line>
                </svg>
            </div>
        </a>
    </div>
    <div class=" mx-auto">
        <div class="block 
             p-6 bg-white border border-gray-200 rounded-lg shadow-md ">

            <img class="rounded h-auto max-w-lg mx-auto" src="{{ asset('storage/kelas-photo/' . $kelas->image) }}"
                alt="image description">

            <h5 class="mb-2 mt-5 text-2xl font-bold tracking-tight text-gray-900 ">{{ $kelas->title }}</h5>
            <span class="bg-blue-100 text-blue-800  text-xs font-medium mr-2 px-2.5 py-0.5 rounded-full">{{$kelas->user->name}}</span>
            <p class="font-normal text-gray-700 ">{{ $kelas->deskripsi }}</p>
            <p class="font-normal text-gray-500 ">{{ date('d-m-y', strtotime($kelas->created_at)) }}</p>
        </div>
    </div>


@endsection
