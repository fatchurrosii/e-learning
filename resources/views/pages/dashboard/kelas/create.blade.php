@extends('layouts.dashboard')

@section('title', 'Tambah Kelas')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Tambah Data Kelas | <a class="cursor-pointer" href="{{URL::previous()}}">Data Kelas </a> <span class="font-semibold"> - Tambah Kelas</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('class.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm cursor-pointer text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="min-h-screen">
        <div class="p-5 pb-20 mt-5 bg-white rounded shadow-sm ">
            <form action="{{ route('class.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="kategori kelas">
                        Kategori Kelas
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-zinc-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="kelas_categories" name="id_kategori_kelas" required>
                            <option selected=true disabled="disabled">Pilih Kategori</option>
                            @foreach ($kategori as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="image">
                        Thumbnail Kelas</label>
                    <input
                        class="block w-full mb-5 text-xs text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-zinc-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                        id="image" type="file" name="image">
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Judul Kelas</label>
                    <input type="text" name="title" value="{{ old('title') }}"
                        class="w-full p-2 mt-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Cth: Kelas Writing" required>
                    @error('title')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Embed Video</label>
                    <div class="flex">
                        <span class="inline-flex items-center p-2 px-3 text-sm text-gray-900 bg-gray-100 border border-r-0 border-gray-300 rounded-l-md ">
                            Youtube.com/
                          </span>
                          <input type="text" name="embed_video" value="{{ old('embed_video') }}"
                              class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                              placeholder="....." required>
                    </div>
                    @error('embed_video')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Deskripsi
                        Kelas</label>
                    <textarea id="message" rows="4" name='deskripsi' value="{{ old('deskripsi') }}"
                        class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-zinc-50 rounded-lg border border-gray-300"
                        placeholder="Deskripsi kelas ..." required>{{ old('deskripsi') }}</textarea>
                    @error('deskripsi')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <button type="submit"
                        class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                        KELAS</button>
                </div>
            </form>
        </div>
    </div>
@endsection
