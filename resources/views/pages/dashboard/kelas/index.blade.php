@extends('layouts.dashboard')

@section('title', 'Kelas')

@section('content')

    <div class="p-5 mt-20 bg-white rounded shadow-sm">
        <div class="flex justify-between py-2 mb-3">
            <div >
                <p class="text-base font-semibold ">
                    Data Kelas
                </p>
                <p class="text-sm text-slate-500">Daftar kelas <span class="text-primary"> Naladhipa Courses</span></p>
            </div>
            @if (Auth::user()->role_id == 3)     
            <div class="col-span-3 mt-2 md:col-span-2">
                <a href="{{ route('class.create') }}"
                    class="w-full p-3 text-white uppercase bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">TAMBAH
                    Kelas</a>
            </div>
            @endif
        </div>
        <div class="grid grid-cols-6 mb-4 md:grid-cols-8 md:gap-4 ">
            <div class="col-span-3 md:col-span-3">
                <form action="{{ route('class.index') }}" method="GET">
                    <input type="text" name="keyword"
                        class="w-full p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white"
                        placeholder="Search...">
                </form>
            </div>
        </div>

        <div class="md:relative sm:rounded-lg">
            <div class="flex items-center justify-between pb-4">

            </div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr class="items-center">
                        <th scope="col" class="px-6 py-3 ">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3 ">
                            Judul Kelas
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Author
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Aksi
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @forelse ($kelas as $key => $item)
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <td class="px-6 py-4">
                                <div class="pl-3">
                                    <p class="text-base font-semibold">{{ $key + 1 }}</p>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $item->title }}</p>
                            </td>
                            <td class="px-6 py-4">
                                <p class="text-base font-medium">{{ $item->user->name }}</p>
                            </td>
                            <td class="inline-flex items-center self-center px-6 py-4 ">
                                @if (Auth::user()->role_id == 1)   
                                <a href="{{route('assign', $item->id)}}" type="button" class="self-center text-white bg-gray-400 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center">Tambah Siswa</a>
                                @endif
                                @if (Auth::user()->role_id == 3)       
                                <button id="dropdownDefaultButton" data-dropdown-toggle="dropdown-{{$item}}"
                                    class="self-center text-white bg-gray-400 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center"
                                    type="button">Aksi <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none"
                                        stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 9l-7 7-7-7"></path>
                                    </svg></button>
                                @endif
                                <!-- Dropdown menu -->
                                <div id="dropdown-{{$item}}"
                                    class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                                    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200"
                                        aria-labelledby="dropdownDefaultButton">
                                        <li>
                                            <a href="{{route('class.show', $item->id)}}" class="block px-4 py-2 hover:bg-gray-100 ">Detail</a>
                                        </li>
                                        @if (Auth::user()->role_id == 1)     
                                        <li>
                                            <a href="{{route('assign_kelas', $item->id)}}" class="block px-4 py-2 hover:bg-gray-100 ">Assign Kelas</a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{ route('class.edit', $item->id) }}" class="block px-4 py-2 ">Edit</a>
                                        </li>
                                        <li>
                                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                                action="{{ route('class.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="w-full px-4 py-2 text-left delete-confirm hover:bg-gray-100 focus:outline-none">
                                                    Delete</button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="p-3 mb-3 text-white bg-red-500 rounded shadow-sm">
                            Data Belum Tersedia!
                        </div>
                    @endforelse

                </tbody>
            </table>
            <div class="mt-2">
                {{ $kelas->withQueryString()->links('vendor.pagination.tailwind') }}
            </div>
        </div>
    </div>
    <script>
        $('.delete-confirm').on('click', function (event) {
           event.preventDefault();
           const url = $(this).attr('href');
           swal({
               title: 'Are you sure?',
               text: 'This record and it`s details will be permanantly deleted!',
               icon: 'warning',
               buttons: ["Cancel", "Yes!"],
               }).then(function(value) {
               if (value) {
               window.location.href = url;
             }
           });
          });
     </script>
@endsection
