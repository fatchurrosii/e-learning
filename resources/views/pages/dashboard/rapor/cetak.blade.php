@include('includes.dashboard.style')
{{-- <head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"></head> --}}
<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font-size: 9pt;
        font-family: 'poppins', 'sans-serif';
        -webkit-print-color-adjust: exact !important
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box
    }

    .my-float {
        margin-top: 22px;
        font-size: 18px !important
    }

    .float {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: #31B4ED;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999
    }

    .div-float {
        display: none
    }

    .page {
        width: 21cm;
        height: 29.7cm;
        ;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: #fff;
        box-shadow: 0 0 5px rgba(0, 0, 0, .1)
    }

    .header {
        padding: 1cm 2cm !important;
        z-index: 999;
        height: 35mm
    }

    .isi {
        padding: .5cm 2cm !important;
        z-index: 99909;
        height: 227mm
    }

    .footer {
        padding: .5cm 2cm !important;
        z-index: 999;
        height: 35mm
    }

    .footer-new {
        padding: .5cm 2cm !important;
        z-index: 999;
        height: 60mm
    }

    @page {
        size: A4;
        margin: 0
    }

    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            page-break-before: always
        }

        .page-landscape {
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            margin: 2cm;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            page-break-before: always;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .div-float {
            display: none
        }
    }

    .icon-img {
        width: 2cm
    }

    @media print {
        #header {
            visibility: hidden;
        }

        .book {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>

<body>
    <div id="header" class="row" style="margin:1cm auto;width: 21cm;">
        <a href="{{route('rapor.index')}}" class="px-4 py-2 bg-white border rounded-full cursor-pointer border-slate-500"
            id="btn-back">Kembali</a>
        <div style="width: 100%;text-align: center;">
            <h1 class="text-xl font-semibold">Rapor Naladhipa Course</h1>
        </div>
    </div>
    <div class="w-full book">
        <div class="page ">
            <div class="header">
                <div class="text-center gap-y-2">
                    <p class="text-3xl font-semibold tracking-wide">Naladhipa Course</p>
                    <p class="text-xl font-semibold tracking-wide">Bimbingan Belajar Bahasa Inggris</p>
                    <p class="text-sm tracking-wide">Alamat Simorejo Sari A Gg XIII no 04, Surabaya, No Telpon 081234809679</p>
                </div>
            </div>
            <div class="mx-auto " style="padding-left: 3rem !important; padding-right: 3rem !important;">
                <hr class="w-full py-2 ">
                <div class="w-6/12 px-4 py-2 text-white bg-purple-500 rounded-xl">
                    <p class="text-xl tracking-wide ">Laporan Hasil Belajar</p>
                </div>
                <div class="py-5 gap-y-2">
                    <p>Nama Peserta Didik&nbsp;&nbsp;&nbsp;: {{$rapor->siswa->user->name}} </p>
                    <p>Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$rapor->siswa->alamat}}
                    </p>
                    <p>Kelas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                            style="margin-left: 2.5px">: {{$rapor->siswa->kelas}}</span></p>
                    <p>Bulan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                            style="margin-left: 1.5px">: {{ Carbon\Carbon::parse($rapor->created_at)->format('F Y')}}</span></p>
                </div>
                <div>
                    <div class="py-5">
                        <table class="w-full border border-collapse border-slate-400">
                            <thead class="text-center bg-purple-100">
                                <tr>
                                    <th class="border border-slate-300">NO</th>
                                    <th class="px-8 border border-slate-300">Exam</th>
                                    <th class="px-8 border border-slate-300">Nilai</th>
                                </tr>
                            </thead>
                            <tbody class="text-sm">
                                <tr>
                                    <td class="text-center border border-slate-300">1</td>
                                    <td class="p-1 text-left border border-slate-300">Nilai Quiz Bulan {{Carbon\Carbon::parse($rapor->created_at)->format('F Y')}}</td>
                                    <td class="text-center border border-slate-300">{{$rapor->rapor_score}}</td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="py-10">
                        <table class="w-full border border-collapse border-slate-400 ">
                            <thead class="text-center bg-purple-100">
                                <tr>

                                    <th class="border border-slate-300">Capaian Belajar Siswa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td class="p-1 text-sm text-left border border-slate-300">{{$rapor->progress_belajar}}</td>

                                </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="grid mt-5 justify-items-end">
                        <div class="text-lg">Surabaya, {{Carbon\Carbon::parse($rapor->created_at)->format('d-m-Y')}}</div>
                        <img class="w-[120px] h-[80px]" src="{{ asset('/assets/img/ttd.jpg') }}" alt="ttd">
                        <div class="text-lg">Albert</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="div-float">
        <a href="javascript:;" class="float" onclick="window.print();">
            <i class="fa fa-print my-float"></i>
        </a>
    </div>
</body>
@include('includes.dashboard.script')
