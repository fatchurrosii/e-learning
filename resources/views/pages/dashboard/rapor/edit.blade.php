@extends('layouts.dashboard')

@section('title', 'Edit Rapor')

@section('content')
<div class="flex justify-between mt-10">
    <span class="self-center text-gray-600">
     Edit Data Rapor| <a class="cursor-pointer" href="{{URL::previous()}}">Data Rapor </a> <span class="font-semibold"> - Edit Rapor</span>
 </span> 
 <div class="col-span-3 mt-2 md:col-span-2">
     <a href="{{ route('rapor.index') }}"
         class="w-full px-3 py-2 border rounded-md shadow-sm cursor-pointer text-dark bg-slate-50 focus:outline-none hover:bg-slate-100"><i class="pr-3 fa-solid fa-arrow-left"></i>Kembali</a>
 </div>
 </div>
    <div class="min-h-screen pb-10 mt-10">
        <div class="p-5 bg-white rounded shadow-sm ">
            <form action="{{ route('rapor.update', $rapor->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" for="role">
                        Siswa
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="siswa" name="siswa_id" required>
                            @foreach ($user as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mt-5">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase" >
                        Mentor
                    </label>
                    <div class="relative">
                        <select
                            class="block w-full px-4 py-3 pr-8 leading-tight text-gray-700 border border-gray-200 rounded appearance-none bg-slate-50 focus:outline-none focus:bg-white focus:border-gray-500"
                            id="mentor" name="mentor_id" required>
                            @foreach ($mentor as $item)
                                <option value="{{ $item->id }}">{{ $item->user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="flex gap-x-10">
                    <div date-rangepicker datepicker-format="yyyy-mm-dd" class="flex items-center mt-5">
                        <div class="relative">
                          <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                              <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path></svg>
                          </div>
                          <input name="start_date" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date start">
                        </div>
                        <span class="mx-4 text-gray-500">to</span>
                        <div class="relative">
                          <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                              <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path></svg>
                          </div>
                          <input name="end_date" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date end">
                      </div>
                    </div>
                    <div class="flex self-center mt-5 gap-x-10 ">
                      <label class="self-center text-xs font-bold tracking-wide text-gray-700 uppercase ">Rapor Score
                          </label>
                      <input disabled type="text" value="{{ old('rapor_score', $rapor->rapor_score) }}"
                          class="p-2 border border-gray-200 rounded shadow-sm bg-zinc-50 focus:outline-none focus:bg-white">
                  </div>
                </div>
                <div class="mt-5 ">
                    <label class="block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase ">Progres Belajar</label>
                    <textarea id="message" rows="4" name='progress_belajar' value="{{ old('progress_belajar', $rapor->progress_belajar) }}"
                        class="block p-2.5 w-full text-sm focus:border-gray-500 text-gray-900 bg-gray-50 rounded-lg border border-gray-300"
                        placeholder="Progres Belajar Siswa ..." required>{{ old('progress_belajar', $rapor->progress_belajar) }}</textarea>
                    @error('progress_belajar')
                        <div class="p-2 mt-2 bg-red-400 rounded shadow-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mt-5">
                    <button type="submit"
                        class="p-2 text-white bg-indigo-500 rounded shadow-sm focus:outline-none hover:bg-indigo-700">SIMPAN
                        RAPOR</button>
                </div>
            </form>
        </div>
    </div>
@endsection
