<!doctype html>
<html class="scroll-smooth" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/flowbite@1.5.3/dist/flowbite.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <title>@yield('title' | 'Naladhipa')</title>
</head>

<body class="antialiased bg-gray-100">

    <div class="relative md:flex">
        @include('components.header-app')


        {{-- index --}}
        <div class="container flex-auto mx-auto">
            <div class="flex flex-col ">

                @yield('content')

            </div>
        </div>


        @stack('before.script')
        @include('includes.dashboard.script')
        @stack('after.script')


</body>


</html>
