<!doctype html>
<html class="scroll-smooth">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <title>Naladhipa Course</title>
    <script src="https://unpkg.com/flowbite@1.5.3/dist/flowbite.js"></script>
</head>

@include('components.header-app')

<body class="antialiased ">

    <section id="#" class="max-w-full pt-24 pb-20 lg:pt-28 md:pt-20 lg:pb-36">
        <div class="container mx-auto ">
            <div class="flex flex-wrap">
                <div class="self-center w-full px-4 lg:px-0 lg:w-1/2">
                    <h1 class="text-3xl font-semibold text-primary md:text-4xl lg:text-4xl">
                        Bimbel Bahasa Inggris untuk anak - anak dengan Mentor yang berpengalaman
                    </h1>
                    <h2 class="py-5 mb-5 text-sm font-medium text-gray-400 lg:text-lg">
                        Dengan bergabung dengan Naladhipa Courses anak anda akan merasakan pengalaman yang seru saat
                        belajar bahasa inggris
                    </h2>
                    <div class="items-center justify-between">
                        <ul class="flex flex-row py-10 space-x-5 lg:md:space-x-10">
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=6281234809679"
                                    class="px-10 py-4 text-xl font-semibold text-white rounded-xl bg-primary">
                                    Yuk Gabung
                                </a>
                            </li>
                            <li class="-mt-3 text-lg font-semibold text-dark">
                                <span class="px-3">10+</span>
                                <br>
                                <span>Siswa</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="self-end w-full px-4 lg:w-1/2 lg:py-0 ">
                    <img src="{{ asset('/assets/img/Hero.png') }}" class="hidden lg:block" alt="">
                </div>
            </div>
        </div>
    </section>

    <section id="feature" name="feature" class="py-5 lg:py-10 lg:pb-20 bg-secondary lg:max-w-full">
        <div class="container mx-auto">
            <div class="py-8 mb-10 text-center lg:px-72 lg:py-20 lg:mb-5">
                <h1 class="text-2xl font-semibold lg:text-4xl text-primary"> Naladhipa Courses merupakan tempat
                    terbaik
                    untuk bimbel
                    Bahasa Inggris </h1>
            </div>
            <div class="flex-wrap px-3 py-5 lg:py-20 lg:flex gap-y-10 lg:px-10">
                <div class="pb-5 lg:w-4/12 md:w-6/12">
                    <div class="max-w-sm p-6 border border-gray-200 shadow-lg bg-primary rounded-xl">
                        <div class="items-center justify-between">
                            <ul class="flex flex-row space-x-5">
                                <li>
                                    <img src="{{ asset('/assets/img/Time-Circle.svg') }}" class="w-16 h-16"
                                        alt="time">
                                </li>
                                <li class="mt-5">
                                    <a href="#">
                                        <h5
                                            class="mb-2 text-2xl font-semibold tracking-tight text-white dark:text-white">
                                            Terjangkau</h5>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <p class="pt-5 mb-3 text-lg font-normal text-white dark:text-gray-400">Dengan harga mulai 100
                            ribu perbulan anak anda sudah bisa mengikuti bimbel disini.</p>

                    </div>

                </div>
                <div class="pb-5 lg:w-4/12 md:w-6/12">
                    <div class="max-w-sm p-6 border border-gray-200 shadow-lg bg-primary rounded-xl">
                        <div class="items-center justify-between">
                            <ul class="flex flex-row space-x-5">
                                <li>
                                    <img src="{{ asset('/assets/img/Time-Circle.svg') }}" class="w-16 h-16"
                                        alt="time">
                                </li>
                                <li class="mt-5">
                                    <a href="#">
                                        <h5
                                            class="mb-2 text-2xl font-semibold tracking-tight text-white dark:text-white">
                                            Flexible</h5>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <p class="pt-5 mb-3 text-lg font-normal text-white dark:text-gray-400">Pembelajaran dengan waktu
                            yang flexible, orang tua dapat memilih jadwal bimbel</p>

                    </div>

                </div>
                <div class="pb-5 lg:w-4/12 md:w-6/12">
                    <div class="max-w-sm p-6 border border-gray-200 shadow-lg bg-primary rounded-xl">
                        <div class="items-center justify-between">
                            <ul class="flex flex-row space-x-5">
                                <li>
                                    <img src="{{ asset('/assets/img/Time-Circle.svg') }}" class="w-16 h-16"
                                        alt="time">
                                </li>
                                <li class="mt-5">
                                    <a href="#">
                                        <h5
                                            class="mb-2 text-2xl font-semibold tracking-tight text-white dark:text-white">
                                            Fun Learning</h5>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <p class="pt-5 mb-3 text-lg font-normal text-white dark:text-gray-400">Dengan menggunakan
                            metode Fun
                            Learning, siswa dapat lebih cepat belajar</p>

                    </div>

                </div>

            </div>
        </div>
    </section>

    <section id="about" name="about" class="py-10 bg-white lg:py-20 ">
        <div class="container mx-auto">
            <div class="px-4 text-left lg:px-0">
                <h1 class="pb-2 text-xl font-medium lg:text-3xl text-primary">
                    Kenalan yuk sama 
                </h1>
                <h1 class="text-3xl font-semibold lg:text-4xl text-primary">
                    Naladhipa Course
                </h1>
            </div>
            <div class="flex-wrap px-3 py-5 lg:px-4 lg:py-10 lg:flex gap-y-10 ">
                <div class="items-center self-center pb-10 lg:w-6/12">
                    <img src="{{ asset('/assets/img/Mentor.png') }}" class="max-w-xs lg:max-w-lg "alt="">
                </div>
                <div class="items-center self-center lg:w-6/12">
                    <h5 class="pb-2 text-lg font-semibold">Naladhipa Course</h5>
                    <h1 class=" text-dark lg:text-2xl">
                     Naladhipa Course merupakan bimbingan belajar yang berfokus pada mata pelajaran Bahasa Inggris, yang dimana dalam pembelajarannya menggunakan metode fun learning. jadi dapat dipastikan pembelajaran di Naladhipa Course sangatlah menyenangkan dan mudah dipahami oleh siswa. 
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <section class="py-10 bg-secondary lg:py-20" id="price" name="price">
        <div class="container mx-auto">
            <div class="px-4 text-left lg:px-0">
                <h1 class="pb-2 text-xl font-semibold lg:text-3xl text-primary">
                    Pilihan Paket
                </h1>
            </div>
            <div class="flex-wrap justify-center px-3 py-5 lg:py-20 lg:flex gap-y-10">
                <div class="pb-10 lg:w-4/12 md:w-6/12">
                    <div class="max-w-sm p-6 bg-white rounded-lg shadow-lg">
                        <div class="items-center">
                            <h1 class="pb-2 text-3xl font-semibold uppercase text-primary">Offline Class</h1>
                            <h2 class="pb-5 text-base font-normal text-gray-400">Benefit kelas</h2>
                        </div>
                        <ul class="grid grid-cols-1 text-sm gap-y-2 lg:gap-y-4 lg:text-base" role="list">
                            <li class="flex items-center text-dark"><svg class="w-6 h-6 mr-4 text-primary"
                                    fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Belajar secara Offline</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Konsultasi belajar</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Akses kelas online</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>4 x Pertemuan / bulan</li>
                        </ul>

                        <hr class="mt-4 mb-4">
                        <h1 class="text-2xl font-medium text-center text-dark"> 100 ribu / Bulan</h1>
                        <div class="flex justify-center mt-5">
                            <a href="https://api.whatsapp.com/send?phone=081703876554&text=Halo%20saya%20ingin%20mendaftar%20paket%20kelas%20offline"
                                class="w-full py-3 font-semibold text-center text-white rounded-xl bg-primary fade hover:bg-purple-700 hover:text-white">
                                Daftar Sekarang</a>
                        </div>
                    </div>
                </div>

                <div class="pb-5 lg:w-4/12 md:w-6/12">
                    <div class="max-w-sm p-6 bg-white rounded-lg shadow-lg">
                        <div class="items-center">
                            <h1 class="pb-2 text-3xl font-semibold uppercase text-primary">Online Class</h1>
                            <h2 class="pb-5 text-base font-normal text-gray-400">Benefit kelas</h2>
                        </div>
                        <ul class="grid grid-cols-1 text-sm gap-y-2 lg:gap-y-4 lg:text-base" role="list">
                            <li class="flex items-center text-dark"><svg class="w-6 h-6 mr-4 text-primary"
                                    fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Belajar secara Online (video call)</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Konsultasi belajar</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>Akses kelas online</li>
                            <li class="flex items-center"><svg class="w-6 h-6 mr-4 text-primary" fill="none"
                                    stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M5 13l4 4L19 7"></path>
                                </svg>8 x Pertemuan / bulan</li>
                        </ul>

                        <hr class="mt-4 mb-4">
                        <h1 class="text-2xl font-medium text-center text-dark"> 150 ribu / Bulan</h1>
                        <div class="flex justify-center mt-5">
                            <a href="https://api.whatsapp.com/send?phone=081703876554&text=Halo%20saya%20ingin%20mendaftar%20paket%20kelas%20online"
                                class="w-full py-3 font-semibold text-center text-white rounded-xl bg-primary fade hover:bg-purple-700 hover:text-white">
                                Daftar Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="py-20 bg-white lg:py-36 " id="testimonials">
        <div class="container mx-auto">
            <div class="flex-wrap lg:flex ">
                <div class="self-center px-4 pb-10 lg:px-0 lg:w-1/2 lg:pb-0 ">
                    <h1 class="pb-5 text-4xl font-medium text-gray-500">Sebuah cerita</h1>
                    <h1 class="text-3xl font-semibold lg:text-6xl text-primary ">Pengalaman saat belajar di Naladhipa
                        Courses</h1>
                </div>
                <div class="self-end w-full mt-5 lg:w-1/2 lg:pl-40">
                    <div class="p-8 bg-white rounded-lg shadow-lg lg:max-w-md">
                        <div class="items-center justify-between">
                            <ul class="flex flex-row space-x-5">
                                <li>
                                    <img src="{{ asset('/assets/img/testi.png') }}" class="w-16 h-16" alt="time">
                                </li>
                                <li class="mt-5">
                                    <h5 class="mb-2 text-2xl font-semibold tracking-normal text-dark ">
                                        Soleh Panjaitan</h5>
                                </li>
                            </ul>
                        </div>
                        <p class="pt-5 mb-3 text-xl font-normal text-gray-400">" Saya sangat senang sekali
                            belajar di Naladhipa Courses karena mentornya sangat ramah dan saya disana belajar dengan
                            menyenangkan karena metode belajarnya sangat mengasyikkan. "</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-10 mb-20 bg-white lg:py-20" id="contact">
        <div class="container mx-auto">
            <div class="flex-wrap lg:flex">
                <div class="self-center px-4 pb-5 lg:px-0 lg:w-1/2 ">
                    <h1 class="pb-5 text-2xl font-semibold lg:text-5xl text-primary">Tertarik untuk
                        belajar ?</h1>
                    <h1 class="mb-10 text-base font-medium text-gray-400 lg:text-xl lg:mb-14">Silahkan segera hubungi
                        kontak
                        dibawah ini untuk konsultasi mengenai bimbingan belajar Naladhipa Courses</h1>
                    <a href="https://api.whatsapp.com/send?phone=6281234809679"
                        class="px-2 py-3 font-semibold text-white rounded-md lg:py-4 bg-primary lg:px-10"><i
                            class="fa-brands fa-whatsapp"></i>Silahkan
                        Hubungi
                        Via Whatsapp</a>
                </div>

                <div class="relative self-end w-full mt-10 lg:w-1/2 lg:pl-20 ">
                    <img src="{{ asset('/assets/img/kontak.svg') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <footer class="w-full py-10 bg-footer-dark lg:py-20" id="footer">
        <div class="container mx-auto">
            <div class="flex-wrap justify-between lg:flex">
                <div class="self-start py-3 pr-32 lg:w-4/12">
                    <h1 class="mb-3 text-2xl font-semibold text-white lg:text-4xl">Naladhipa Courses</h1>
                    <p class="text-base text-white lg:text-lg font-regular">Merupakan bimbel bahasa inggris yang
                        menghadirkan konsep
                        fun learning dalam pembelajarannya</p>
                </div>
                <div class="py-3 lg:w-2/12">
                    <h1 class="mb-2 text-2xl font-semibold text-white lg:text-4xl">Follow us
                    </h1>
                    <ul class="flex flex-col flex-wrap space-y-1">
                        <li>
                            <a href="" class="text-lg text-white">Instagram</a>
                        </li>
                        <li>
                            <a href="" class="text-lg text-white">Facebook</a>
                        </li>
                    </ul>
                </div>
                <div class="py-3 lg:w-2/12">
                    <h1 class="mb-2 text-2xl font-semibold text-white lg:text-4xl">Kelas
                    </h1>
                    <ul class="flex flex-col flex-wrap space-y-1">
                        <li>
                            <a href="" class="text-lg text-white">Offline Class</a>
                        </li>
                        <li>
                            <a href="" class="text-lg text-white">Online Class</a>
                        </li>
                    </ul>
                </div>
                <div class="py-3 lg:w-3/12">
                    <h1 class="mb-2 text-2xl font-semibold text-white lg:text-4xl">Alamat</h1>
                    <p class="text-lg text-white text-semibold lg:text-2xl">Simorejo Sari A Gg XIII no 04, Surabaya Jawa
                        Timur, Indonesia
                    </p>
                    <p class="text-lg font-medium text-white">081234809679</p>
                </div>
            </div>
        </div>

    </footer>
</body>

</html>
