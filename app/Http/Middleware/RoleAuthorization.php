<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next ,...$roles)
    {
        $roleAuth = auth()->user()->role;

        if (count($roles) > 0) {
            if (in_array($roleAuth->name, $roles)) {
                return $next($request);
            } else {
                return redirect('/')->with(['error' => 'Anda tidak mempunyai akses ke halaman ini', 403]);
            }
        } else {
            return $next($request);
        }
    }
}
