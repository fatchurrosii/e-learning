<?php

namespace App\Http\Controllers;

use App\Models\KategoriKelas;
use App\Models\Kelas;
use App\Models\KelasStudent;
use App\Models\Mentor;
use App\Models\Siswa;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $keyword = $request->keyword;
        if(Auth::user()->role_id == 3){
            $kelas  = Kelas::with(['kategori_kelas', 'user'])->where('user_id', $user->id)
                ->whereHas('user', function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                })->paginate(5);
                return view('pages.dashboard.kelas.index', compact('kelas'));
            }
            $kelas  = Kelas::with(['kategori_kelas', 'user'])->where('title', 'LIKE', '%' . $keyword . '%')->paginate(5);
            return view('pages.dashboard.kelas.index', compact('kelas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $kategori = KategoriKelas::all();
        $kelas = Kelas::all();
        return view('pages.dashboard.kelas.create', compact(['kategori', 'kelas']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        $this->validate($request, [
            'title' => 'required',
            'embed_video' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg'
        ]);
        $image  = $request->file('image');
        $image->storeAs('kelas-photo', $image->hashName());

        DB::beginTransaction();
        try {
            $kelas = new Kelas();
            $kelas->id_kategori_kelas = $request->id_kategori_kelas;
            $kelas->title = $request->title;
            $kelas->embed_video = $request->embed_video;
            $kelas->deskripsi = $request->deskripsi;
            $kelas->image = $image->hashName();
            $user->kelas()->save($kelas);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }


        if ($kelas) {
            return redirect()->route('class.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('class.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelas = Kelas::with(['kategori_kelas', 'user'])->findOrFail($id);
        return view('pages.dashboard.kelas.show', compact('kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::with(['kategori_kelas', 'user'])->findOrFail($id);
        $kategori_kelas = KategoriKelas::select('id', 'name')->get();
        return view('pages.dashboard.kelas.edit', compact(['kelas', 'kategori_kelas']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $this->validate($request, [
            'title' => 'required',
            'embed_video' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        $kelas =  Kelas::findOrFail($id);
        if ($request->file('photo') == "") {
            $kelas->id_kategori_kelas = $request->id_kategori_kelas;
            $kelas->title = $request->title;
            $kelas->embed_video = $request->embed_video;
            $kelas->deskripsi = $request->deskripsi;
            $user->kelas()->save($kelas);
        } else {
            Storage::disk('local')->delete('kelas-photo/' . $kelas->image);
            $image = $request->file('image');
            $image->storeAs('kelas-photo', $image->hashName());
            $kelas->image = $image->hashName();
            $kelas->id_kategori_kelas = $request->id_kategori_kelas;
            $kelas->title = $request->title;
            $kelas->embed_video = $request->embed_video;
            $kelas->deskripsi = $request->deskripsi;
            $user->kelas()->save($kelas);
        };
        return redirect()->route('class.index')->with(['success' => 'Data berhasil diubah !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->delete();
        return redirect()->route('class.index')->with(['success' => 'Data berhasil dihapus!']);
    }
    public function assign_kelas($kelasId){
        $kelas = Kelas::findOrFail($kelasId);
        $user_id = $kelas->user_id;
        $mentor_id = Mentor::where('user_id', $user_id)->first();
        $siswa = Siswa::where('mentor_id', $mentor_id->id)->whereHas('siswa_kelas', function($q) use($kelas){
            $q->where('kelas_id', $kelas->id);
            $q->where('siswa_id', '<>', 'siswa.id');
        })->get();
        $siswa = Siswa::where('mentor_id', $mentor_id->id)->whereNotIn('id', $siswa->modelKeys())->whereHas('user', function($query){
            $query->where('status', 'Active');
        })->get();
        return view('pages.dashboard.kelas.assign', compact(['kelas', 'siswa']));
    }

    public function assign(Request $request,$kelasId){
        $kelas = Kelas::findOrFail($kelasId);
        DB::beginTransaction();
        try{
            if((is_array($request->siswa_ids))){
                foreach($request->siswa_ids as $key => $siswaIds){
                    $kelasStudent = new KelasStudent();
                    $kelasStudent->kelas_id = $kelas->id;
                    $kelasStudent->siswa_id = $siswaIds;
                    $kelasStudent->save();
                }
            }
            DB::commit();
            return redirect()->route('class.index')->with(['success' => 'Data berhasil disimpan!']);
        }catch(Exception $e){
            DB::rollBack();
            return $e;
        }
    }
}
