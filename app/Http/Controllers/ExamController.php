<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\ExamQuestion;
use App\Models\ExamStudent;
use App\Models\ExamUser;
use App\Models\Mentor;
use App\Models\Question;
use App\Models\Siswa;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user()->id;
        if(Auth::user()->role_id == 3){
            $exam = Exam::where('name', 'LIKE', '%' . $request->keyword . '%')->where('created_by', $user)->paginate(5);
    
            return view('pages.dashboard.quiz.exam.index', compact('exam'));
        }
        $exam = Exam::where('name', 'LIKE', '%' . $request->keyword . '%')->paginate(5);
    
        return view('pages.dashboard.quiz.exam.index', compact('exam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user()->id;
        $question = Question::where('created_by', $user)->get();
        return view('pages.dashboard.quiz.exam.create', compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|string',
            'image' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        DB::beginTransaction();
        try {

            $image  = $request->file('image');
            $image->storeAs('exam-image', $image->hashName());
            $exam = new Exam();
            $exam->name = $request->title;
            $exam->image = $image->hashName();
            $exam->created_by = Auth::id();
            $exam->save();

            if ((is_array($request->questions))) {
                foreach ($request->questions as $key => $examQuestion) {

                    // $examArray = array(
                    $eQuestion = new ExamQuestion();
                    $eQuestion->exam_id = $exam->id;
                    $eQuestion->question_id  = $examQuestion;
                    // );
                    $eQuestion->save();
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
        return redirect()->route('exam.index')->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::find($id);
        $question = Question::with('question_options')->whereHas('exam_question', function ($q) use ($exam) {
            $q->where('exam_id', $exam->id);
        })->with([
            'question_options', 'quiz_texts', 'quiz_images', 'quiz_videos', 'quiz_sounds'
        ])->paginate(1);
        return view('pages.dashboard.quiz.exam.show', compact(['exam', 'question']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::with('exam_question.question.question_options')->find($id);
        $questions = Question::all();
        return view('pages.dashboard.quiz.exam.edit', compact(['exam', 'questions']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam = Exam::find($id);

        DB::beginTransaction();
        try {
            $exam->name = $request->title;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image->storeAs('exam-image', $image->hashName());
                $exam->image = $image->hashName();
            }
            $exam->created_by = Auth::id();
            $exam->save();

            if (is_array($request->questions)) {
                $exam->exam_question()->delete();
                foreach ($request->questions as $key => $examQuestion) {
                    $eQuestion = new ExamQuestion();
                    $eQuestion->exam_id = $exam->id;
                    $eQuestion->question_id = $examQuestion;
                    $eQuestion->save();
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
        return redirect()->route('exam.index')->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::findOrFail($id);
        $exam->exam_question()->delete();
        $exam->delete();
        return redirect()->route('exam.index')->with(['success' => 'Data berhasil dihapus!']);
    }
    
    public function assign_exam($examId){
        $exam = Exam::findOrFail($examId);
        $user_id = $exam->created_by;
        $mentor = Mentor::where('user_id', $user_id)->first();
        $siswa = Siswa::where('mentor_id', $mentor->id)->whereHas('exam_students', function($q) use ($exam) {
            $q->where('exam_id', $exam->id);
            $q->where('siswa_id', '<>', 'siswa.id');
        })->get();
        $siswa = Siswa::where('mentor_id', $mentor->id)->whereNotIn('id', $siswa->modelKeys())->whereHas('user', function($query){
            $query->where('status', 'Active');
        })->get();
        return view ('pages.dashboard.quiz.exam.assign', compact(['exam', 'siswa']));
    }
    
    public function student (Request $request, $examId){
        $this->validate($request, [
            'siswa_ids' => 'required',
            'siswa_ids.*'
        ]);
        $exam = Exam::findOrFail($examId);
        DB::beginTransaction();
        try{
            if((is_array($request->siswa_ids))){
                foreach($request->siswa_ids as $key => $siswaIds){
                    ExamStudent::create([
                        'exam_id' => $exam->id,
                        'siswa_id' => $siswaIds
                    ]);
                }
            }else{
            echo "error";
            }
            DB::commit();
            return redirect()->route('exam.index')->with(['success' => 'Data berhasil disimpan!']);
        }catch(Exception $e){
            DB::rollBack();
            return $e;
        }
    }
}
