<?php

namespace App\Http\Controllers;

use App\Models\KategoriSiswa;
use App\Models\Mentor;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword;
        $siswa = Siswa::with(['kategori_siswa', 'user'])
            ->whereHas('user', function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            })->paginate(5);

        // dd($siswa);
        return view('pages.dashboard.siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $siswa = Siswa::all();
        // $user = User::where('id_role', '3')->get();      

        $user = User::where('role_id', '2')
            ->whereHas('siswa', function ($q) {
                $q->where('user_id', '<>', 'user.id');
            })->get();

        $user = User::whereNotIn('id', $user->modelKeys())
            ->where('role_id', '2')->get();

        $mentor = Mentor::all();


        return view('pages.dashboard.siswa.create', compact([ 'user', 'siswa', 'mentor']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [


            'alamat' => 'required|max:255',
            'photo' => 'required|image|mimes:png,jpg,jpeg',
            'no_hp' => 'required|numeric|min:11',
            'kelas' => 'required'
        ]);

        $photo  = $request->file('photo');
        $photo->storeAs('siswa-photo', $photo->hashName());
        $siswa =  Siswa::firstOrCreate(
            [
                'user_id' => $request->user_id,
            ],
            [
                'user_id' => $request->user_id,
                'mentor_id' => $request->mentor_id,
                'alamat' => $request->alamat,
                'photo' => $photo->hashName(),
                'no_hp' => $request->no_hp,
                'kelas' => $request->kelas
            ]
        );

        if ($siswa) {

            return redirect()->route('siswa.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('siswa.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        $kategori_siswa = KategoriSiswa::select('id', 'name')->get();
        $mentor = Mentor::all();
        $user = User::where('role_id', 2)->get();
        // dd($user->modelKeys());

        return view('pages.dashboard.siswa.edit', compact(['siswa', 'kategori_siswa', 'user','mentor']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [

            'alamat' => 'required|max:255',
            'photo' => 'nullable|image|mimes:png,jpg,jpeg',
            'no_hp' => 'required',
            'kelas' => 'required'
        ]);
        $siswa = Siswa::findOrFail($id);
        if ($request->file('photo') == "") {
            $siswa->alamat = $request->alamat;
            $siswa->mentor_id = $request->mentor_id;
            $siswa->no_hp = $request->no_hp;
            $siswa->kelas = $request->kelas;
            $siswa->save();
        } else {
            Storage::disk('local')->delete('siswa-photo/' . $siswa->photo);
            $photo  = $request->file('photo');
            $photo->storeAs('siswa-photo', $photo->hashName());
            $siswa->photo = $photo->hashName();
            $siswa->mentor_id = $request->mentor_id;
            $siswa->alamat = $request->alamat;
            $siswa->no_hp = $request->no_hp;
            $siswa->kelas = $request->kelas;
            $siswa->save();
        }

        return redirect()->route('siswa.index')->with(['success' => 'Data berhasil diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();
        return redirect()->route('siswa.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
