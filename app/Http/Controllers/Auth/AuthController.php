<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yoeunes\Toastr\Facades\Toastr;

class AuthController extends Controller
{
    public function Login(Request $request)
    {
        return view('login', [
            'title' => 'Login'
        ]);
    }
    public function register(){
        return view('register', [
            'title' => 'Register'
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            
            if (Auth::user()->status ==  'Active') {
                $request->session()->regenerate();
                if (Auth::user()->role_id != 3) {
                    return redirect('/dashboard')->with(['success' => 'Login berhasil']);;
                }
                return redirect('/')->with(['success' => 'Login berhasil']);
            }
            Auth::logout();
            return redirect('/login')->with(['error' => 'Akun anda belum aktif, silahkan hubungi Admin']);
        }elseif(!Auth::attempt($credentials)){
            return redirect('/login')->with(['error' => 'Masukkan email dan password dengan benar'] );
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required|confirmed',
            'new_password_confirmation' => 'required'
        ]);

        $user = User::find(auth()->user()->id);

        if (!empty($user)) {
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                return redirect()->back()->with(['success' => 'Password telah berhasil diubah']);
            }
            return redirect()->back()->with(['error' => 'Password tidak sesuai']);
        }
        return redirect()->back()->with(['error' => 'User tidak ditemukan']);
    }

    public function registerUser(Request $request){
      
        $this->validate($request, [
        'name' => 'required',
        'email' => 'required|unique:users|email',
        'password' => 'required|confirmed',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = 2;
        $user->status = 'Inactive';
        $user->save();
        $siswa = new Siswa();
        $siswa->user_id = $user->id;
        $user->kelas = 'Online';
        $siswa->save();
        if($user){
            return redirect()->route('login')->with(['success' => 'User telah berhasil dibuat']);
        }
    }
}
