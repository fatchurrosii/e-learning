<?php

namespace App\Http\Controllers;

use App\Models\QuizVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuizVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $user = Auth::user()->id;
        $video = QuizVideo::where('created_by', $user)->paginate();
        return view('pages.dashboard.quiz.video.index', compact('video'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request);
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'video' => 'required'
        ]);
        $quiz = new QuizVideo();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->video = $request->video;
        $quiz->created_by = Auth::user()->id;
        $quiz->save();

        if ($quiz) {
            return redirect()->route('quiz-video.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-video.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = QuizVideo::find($id);
        return view('pages.dashboard.quiz.video.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'video' => 'nullable'
        ]);
        $quiz = QuizVideo::find($id);
        $quiz = new QuizVideo();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->video = $request->video;
        $quiz->save();
        if ($quiz) {
            return redirect()->route('quiz-video.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-video.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = QuizVideo::find($id);
        $quiz->delete();
        return redirect()->route('quiz-video.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
