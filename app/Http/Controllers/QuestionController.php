<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\QuizImage;
use App\Models\QuizOption;
use App\Models\QuizSound;
use App\Models\QuizText;
use App\Models\QuizVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   $user = Auth::user()->id;
        $question = Question::where('created_by', $user)->where('name', 'LIKE', '%'. $request->keyword .'%')->paginate(5);
        return view('pages.dashboard.quiz.questions.index', compact('question'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $user = Auth::user()->id;
        $quizImage = QuizImage::where('created_by', $user)->get();
        $quizText = QuizText::where('created_by', $user)->get();
        $quizSound = QuizSound::where('created_by', $user)->get();
        $quizVideo = QuizVideo::where('created_by', $user)->get();

        return view('pages.dashboard.quiz.questions.create', compact('quizImage','quizText','quizSound','quizVideo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'score' => 'required|numeric|min:10|max:100',
            'quiz_image_id' => 'nullable',
            'quiz_text_id' => 'nullable',
            'quiz_sound_id' => 'nullable',
            'quiz_video_id' => 'nullable',
        ]);

        $question = new Question();
        $question->name = $request->name;
        $question->score = $request->score;
        $question->quiz_images_id = $request->images_id;
        $question->quiz_text_id = $request->text_id;
        $question->quiz_sounds_id = $request->sounds_id;
        $question->quiz_videos_id = $request->videos_id;
        $question->created_by = Auth::id();
        $question->save();

        for($q =1 ; $q <= 4; $q++){
            $option = $request->input('option_text'. $q, '');
            if($option != ''){
                QuizOption::create([
                    'question_id' => $question->id,
                    'option_text' => $option,
                    'correct' => $request->input('correct' .$q) ? 1 : 0,
                    'created_by' => Auth::user()->id
                ]);
            }
        }
        return redirect()->route('question.index')->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::with(['question_options'])->find($id);
        $quizImage = QuizImage::all();
        $quizText = QuizText::all();
        $quizSound = QuizSound::all();
        $quizVideo = QuizVideo::all();
        return view('pages.dashboard.quiz.questions.edit', compact('question','quizImage','quizText','quizSound','quizVideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
{
    $request->validate([
        'name' => 'required',
        'score' => 'required|numeric|min:10|max:100',
    ]);

    $question = Question::findOrFail($id);
    $question->name = $request->name;
    $question->score = $request->score;
    $question->quiz_images_id = $request->images_id;
    $question->quiz_text_id = $request->text_id;
    $question->quiz_sounds_id = $request->sounds_id;
    $question->quiz_videos_id = $request->videos_id;
    $question->save();

    $question->question_options()->delete();

    for ($q = 1; $q <= 4; $q++) {
        $option_text = $request->input('option_text' . $q);
        $correct = $request->input('correct' . $q) ? true : false;
        $option = new QuizOption();
        $option->question_id = $question->id;
        $option->option_text = $option_text;
        $option->correct = $correct;
        $option->save();
    }

    return redirect()->route('question.index')->with(['success' => 'Data berhasil diupdate!']);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();
        $question->question_options()->delete();
        return redirect()->route('question.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
