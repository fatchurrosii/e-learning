<?php

namespace App\Http\Controllers;

use App\Models\QuizImage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class QuizImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;
        $image = QuizImage::where('created_by', $user)->paginate();
        return view('pages.dashboard.quiz.images.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'images' => 'required|image|mimes:png,jpg,jpeg'
        ]);
        $image  = $request->file('images');
        $image->storeAs('quiz-image', $image->hashName());

        $quiz = new QuizImage();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->images = $image->hashName();
        $quiz->created_by = Auth::user()->id;
        $quiz->save();

        if ($quiz) {
            return redirect()->route('quiz-image.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-image.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = QuizImage::find($id);
        return view('pages.dashboard.quiz.images.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'images' => 'nullable|image|mimes:png,jpg,jpeg'
        ]);

        $quiz = QuizImage::find($id);

        if($request->file('images') == ""){
            $quiz->title = $request->title;
            $quiz->caption = $request->caption;
            $quiz->save();
        }else{
            Storage::disk('local')->delete('quiz-image/' . $quiz->images);
            $image  = $request->file('images');
            $image->storeAs('quiz-image', $image->hashName());
            $quiz->title = $request->title;
            $quiz->caption = $request->caption;
            $quiz->images = $image->hashName();
            $quiz->save();
        }

        if ($quiz) {
            return redirect()->route('quiz-image.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-image.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rapor = QuizImage::findOrFail($id);
        $rapor->delete();
        return redirect()->route('quiz-image.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
