<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Rapor;
use App\Models\Siswa;
use App\Models\TestResult;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf as FacadePdf;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(){
        $user = auth()->user()->id;
        $siswa = Siswa::with('user.test_result','kategori_siswa')->where('user_id', $user)->first();
        $test = TestResult::with('exam')->where('user_id', $user)->paginate(5);
        return view('pages.landing.profile.index', compact(['siswa','test']));
    }
    public function header (){
        $user = auth()->user();
        $siswa = Siswa::where('user_id', $user->id)->first();
        return view('components.header-app', compact('siswa'));
    }
    public function edit(){
        $user = auth()->user();
        $siswa = Siswa::with('user')->where('user_id', $user->id)->first();
        return view('pages.landing.profile.edit', compact(['siswa', 'user']));
    }
    
    public function update(Request $request){

        $user = auth()->user();
        $siswa = Siswa::where('user_id', $user->id)->first();
        if ($request->file('photo') == "") {
            $user->name = $request->name;
            $user->save();
            $siswa->alamat = $request->alamat;
            $siswa->no_hp = $request->no_hp;
            $siswa->save();
        }else{
            Storage::disk('local')->delete('siswa-photo/' . $siswa->photo);
            $photo  = $request->file('photo');
            $photo->storeAs('siswa-photo', $photo->hashName());
            $siswa->photo = $photo->hashName();
            $siswa->alamat = $request->alamat;
            $siswa->no_hp = $request->no_hp;
            $siswa->save();
        }

        return redirect()->route('profile')->with(['success' => 'Profile berhasil diupate']);
    }
    public function cetak($raporId){
        ini_set('max_execution_time', 0);
       $rapor = Rapor::select([
        'id',
        'speaking',
        'siswa_id',
        'reading',
        'listening',
        'progress_belajar',
        'reading_note',
        'listening_note',
        'speaking_note'
       ])->with('siswa')->find($raporId);
        $pdf = FacadePdf::loadview('cetak', ['rapor' => $rapor]);
     
        return $pdf->download('rapor.pdf');
    }

    public function cetakSiswa(){
        $siswa = Siswa::all();

        $pdf= FacadePdf::loadView('cetak-siswa', ['siswa' => $siswa]);
       return $pdf->download('siswa.pdf');
    }
    
    public function resetPassword(Request $request)
    {   
        $user = auth()->user();
        // dd($user);
        $user->password = Hash::make(env('DEFAULT_PASSWORD'));
        $user->save();
        return redirect()->route('profile')->with(['success' => 'Password telah berhasil diubah ke default']);
    }
}
