<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Kelas;
use App\Models\KelasStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HalamanKelasController extends Controller
{
    public function index()
    {
        $speaking = Kelas::with('user')->whereHas('kategori_kelas', function ($q) {
            $q->where('name', 'Speaking');
        })->whereHas('kelas_students', function($q){
            $q->where('siswa_id', Auth::user()->student->id);
        })->get();
        $reading = Kelas::with('user')->whereHas('kategori_kelas', function ($q) {
            $q->where('name', 'Reading');
        })->whereHas('kelas_students', function($q){
            $q->where('siswa_id', Auth::user()->student->id);
        })->get();
        $writing = Kelas::with('user')->whereHas('kategori_kelas', function ($q) {
            $q->where('name', 'Writing');
        })->whereHas('kelas_students', function($q){
            $q->where('siswa_id', Auth::user()->student->id);
        })->get();
        return view('pages.landing.kelas.index', compact(['speaking', 'reading', 'writing']));
    }

    public function show($kelasId)
    {
        $kelasVideo = Kelas::with(['user', 'kategori_kelas'])->findOrFail($kelasId);
        return view('pages.landing.kelas.show', compact('kelasVideo'));
    }

    public function changeStatus($kelasId)
    {
        $kelasVideo = Kelas::findOrFail($kelasId);
        $kelasStudent = KelasStudent::where('kelas_id', $kelasVideo->id)->first();
        $kelasStudent->is_done = true;
        $kelasStudent->save();
        return redirect()->route('list.kelas')->with(['success' => 'Sudah Ditonton']);
    }
}
