<?php

namespace App\Http\Controllers;

use App\Models\QuizText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuizTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $user = Auth::user()->id;
        $text = QuizText::where('created_by', $user)->paginate();
        return view('pages.dashboard.quiz.text.index', compact('text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'text' => 'required'
        ]);
        $quiz = new QuizText();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->text = $request->text;
        $quiz->created_by = Auth::user()->id;
        $quiz->save();
        if ($quiz) {
            return redirect()->route('quiz-text.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-text.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $text = QuizText::find($id);
        return view('pages.dashboard.quiz.text.edit', compact('text'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'text' => 'required'
        ]);
        $quiz = QuizText::find($id);
        $quiz = new QuizText();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->text = $request->text;
        $quiz->save();
        if ($quiz) {
            return redirect()->route('quiz-text.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-text.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = QuizText::find($id);
        $quiz->delete();
        return redirect()->route('quiz-text.index')->with(['success' => 'Data berhasil dihapus!']);
    }
    
}
