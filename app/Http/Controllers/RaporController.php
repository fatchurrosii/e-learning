<?php

namespace App\Http\Controllers;

use App\Http\Resources\RaporResource;
use App\Models\Exam;
use App\Models\Mentor;
use App\Models\Rapor;
use App\Models\Siswa;
use App\Models\TestResult;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RaporController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        $keyword = $request->keyword;
        $rapor = Rapor::with(['siswa', 'mentor'])
            ->whereHas('siswa', function ($query) use ($keyword) {
                $query->whereHas('user', function($q) use($keyword) {
                    $q->where('name','LIKE', '%' . $keyword . '%' );
                });
            })->paginate(5);
        return view('pages.dashboard.rapor.index', compact('rapor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $siswa = User::where('role_id', 2)->get();
        $mentor = Mentor::all();
        
        return view('pages.dashboard.rapor.create', compact(['siswa', 'mentor']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'user_id' => 'required',
            'progress_belajar' => 'required',
            'mentor_id' => 'required'
        ]);
            $siswa = Siswa::where('user_id', $request->user_id)->first();
            $score = TestResult::where('user_id', $request->user_id)->whereBetween('created_at', [Carbon::createFromFormat('Y-m-d', $request->start_date), Carbon::createFromFormat('Y-m-d', $request->end_date)])->sum('score');       
            $rapor = new Rapor();
            $rapor->siswa_id = $siswa->id;
            $rapor->progress_belajar = $request->progress_belajar;
            $rapor->mentor_id = $request->mentor_id;
            $rapor->rapor_score = $score;
            $rapor->save();
          
        if ($rapor) {
            return redirect()->route('rapor.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('rapor.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $rapor = Rapor::findOrFail($id);
        $user = User::where('role_id', 2)->get();
        $mentor = Mentor::all();
        return view('pages.dashboard.rapor.edit', compact(['user', 'mentor', 'rapor']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'siswa_id' => 'required',
            'progress_belajar' => 'required',
            'mentor_id' => 'required'
        ]);
        $rapor = Rapor::find($id);
            $siswa = Siswa::where('user_id', $request->user_id)->first();
            $score = TestResult::where('user_id', $request->user_id)->whereBetween('created_at', [Carbon::createFromFormat('Y-m-d', $request->start_date), Carbon::createFromFormat('Y-m-d', $request->end_date)])->sum('score');
            
            $rapor = new Rapor();
            $rapor->siswa_id = $siswa->id;
            $rapor->rapor_score = $score;
            $rapor->progress_belajar = $request->progress_belajar;
            $rapor->mentor_id = $request->mentor_id;
            $rapor->save();
          
        if ($rapor) {
            return redirect()->route('rapor.index')->with(['success' => 'Data berhasil diupdate!']);
        } else {
            return redirect()->route('rapor.index')->with(['error' => 'Data gagal diupdate!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rapor = Rapor::findOrFail($id);
        $rapor->delete();
        return redirect()->route('rapor.index')->with(['success' => 'Data berhasil dihapus!']);
    }

    public function cetak($id){
        $rapor = Rapor::with(['siswa','mentor'])->findOrFail($id);
        return view('pages.dashboard.rapor.cetak', compact('rapor'));
        
    }
}
