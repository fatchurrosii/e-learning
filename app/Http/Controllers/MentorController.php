<?php

namespace App\Http\Controllers;

use App\Models\Mentor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword;
        $mentor  = Mentor::with(['user'])
            ->whereHas('user', function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            })->paginate(5);

        // dd($siswa);
        return view('pages.dashboard.mentor.index', compact('mentor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $mentor = Mentor::all();
        // $user = User::where('id_role', '3')->get();      

        $user = User::where('role_id', '3')
            ->whereHas('mentor', function ($q) {
                $q->where('user_id', '<>', 'user.id');
            })->get();

        $user = User::whereNotIn('id', $user->modelKeys())
            ->whereHas('role', function($q){
                $q->where('name', 'Mentor');
            })->get();

        return view('pages.dashboard.mentor.create', compact(['user', 'mentor']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [

            'user_id' => 'required',
            'alamat' => 'required|max:255',
            'photo' => 'required|image|mimes:png,jpg,jpeg',
            'no_hp' => 'required|numeric|min:11',
        ]);

        $photo  = $request->file('photo')->store('mentor-photo');
    //    $path = $photo->store('mentor-photo', $photo->hashName());


        $mentor =  Mentor::firstOrCreate(
            [
                'user_id' => $request->user_id,
            ],
            [
                'user_id' => $request->user_id,
                'alamat' => $request->alamat,
                'photo' => $photo,
                'no_hp' => $request->no_hp,
            ]
        );

        if ($mentor) {

            return redirect()->route('mentor.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('mentor.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mentor = Mentor::findOrFail($id);
        $user = User::where('role_id', '3')->get();
        // dd($user->modelKeys());

        return view('pages.dashboard.mentor.edit', compact(['mentor', 'user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'alamat' => 'required|max:255',
            'photo' => 'required|image|mimes:png,jpg,jpeg',
            'no_hp' => 'required',
        ]);
        
        $mentor = Mentor::find($id);
        if (!$mentor) {
            return redirect()->route('mentor.index')->with(['error' => 'Mentor not found!']);
        }
        
        $mentor->alamat = $request->alamat;
        $mentor->no_hp = $request->no_hp;
        
        if ($request->hasFile('photo')) {
            // Delete existing photo if present
            Storage::disk('local')->delete('mentor-photo/' . $mentor->photo);
        
            // Store the new photo
            $photo = $request->file('photo');
            $photoPath = $photo->storeAs('mentor-photo', $photo->hashName());
            $mentor->photo = $photoPath;
        }
        
        $mentor->save();
        
        return redirect()->route('mentor.index')->with(['success' => 'Data berhasil diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Mentor::findOrFail($id);
        $siswa->delete();
        return redirect()->route('mentor.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
