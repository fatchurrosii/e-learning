<?php

namespace App\Http\Controllers;

use App\Models\KategoriSiswa;
use Illuminate\Http\Request;

class KategoriSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $siswaCategories = KategoriSiswa::get();
        return view('pages.dashboard.siswa.kategori.index', compact('siswaCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.dashboard.siswa.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        KategoriSiswa::create(['name' => $request->name]);


        return redirect()->route('kategoriSiswa.index')->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswaCategories = KategoriSiswa::findOrFail($id);
        return view('pages.dashboard.siswa.kategori.edit', compact('siswaCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $siswaCategories = KategoriSiswa::findorFail($id);
        $siswaCategories->name = $request->name;
        $siswaCategories->save();

        return redirect()->route('kategoriSiswa.index')->with(['success' => 'Data berhasil diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = KategoriSiswa::findOrFail($id);
        $siswa->delete();
        return redirect()->route('siswa.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
