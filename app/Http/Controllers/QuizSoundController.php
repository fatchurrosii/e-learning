<?php

namespace App\Http\Controllers;

use App\Models\QuizSound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class QuizSoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;
        $sound = QuizSound::where('created_by', $user)->paginate();
        return view('pages.dashboard.quiz.sound.index', compact('sound'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'path' => 'required|mimes:mpeg,mpga,mp3,wav'
        ]);
        $file  = $request->file('path');
        $filename = $file->getClientOriginalName();
       $path = $file->storeAs('quiz-sound', $filename);
        $quiz = new QuizSound();
        $quiz->title = $request->title;
        $quiz->caption = $request->caption;
        $quiz->path = $path;
        $quiz->created_by = Auth::user()->id;
        $quiz->save();
        
        if ($quiz) {
            return redirect()->route('quiz-sound.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('quiz-sound.index')->with(['error' => 'Data gagal disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sound = QuizSound::find($id);
        return view('pages.dashboard.quiz.sound.edit', compact('sound'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'caption' => 'required',
            'path' => 'nullable|mimes:mp3|max:2048'
        ]);

        $quiz = QuizSound::find($id);

        if($request->file('path') == ""){
            $quiz->title = $request->title;
            $quiz->caption = $request->caption;
            $quiz->save();
        }else{
            Storage::disk('local')->delete('quiz-sound/' . $quiz->images);
            $file  = $request->file('path');
            $filename = $file->getClientOriginalName();
           $path = $file->storeAs('quiz-sound', $filename);
            $quiz->title = $request->title;
            $quiz->caption = $request->caption;
            $quiz->path = $path;
            $quiz->save();
        }
        return redirect()->route('quiz-sound.index')->with(['success' => 'Data berhasil diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = QuizSound::find($id);
        $quiz->delete();
        return redirect()->route('quiz-sound.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
