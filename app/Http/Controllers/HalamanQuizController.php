<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Question;
use App\Models\QuizOption;
use App\Models\TestResult;
use Illuminate\Http\Request;

class HalamanQuizController extends Controller
{
    public function index(){
        $user = auth()->user()->student->id;
        $quiz = Exam::with('test_result')->whereHas('exam_students', function($q) use($user){
            $q->where('siswa_id', $user);
        })->get();
        return view('pages.landing.exam.index', compact('quiz'));
    }

    public function show($examId){
        $exam = Exam::find($examId);
        $question = Question::whereHas('exam_question', function ($q) use ($exam){
            $q->where('exam_id', $exam->id); 
        })->with('question_options', 'quiz_images', 'quiz_texts', 'quiz_videos', 'quiz_sounds',)->paginate();
        $test_result = null;
        if($exam->test_result){
            $test_result = TestResult::where('exam_id', $exam->id)->where('user_id', auth()->user()->id)->latest()->first();
        }
        return view('pages.landing.exam.show', compact('question', 'exam', 'test_result'));
    }

    public function test(Request $request, $examId){
        $exam = Exam::findOrFail($examId);
        $score = 0;

        foreach($request->question as $question_id => $answer_id){
            $question = Question::find($question_id);
            $correct = QuizOption::where('question_id', $question_id)
                        ->where('id', $answer_id)
                        ->where('correct', 1);
            if($correct->count() > 0){
                $score += $question->score;
            }
        }
        // dd($score);

         TestResult::create([
            'exam_id' => $exam->id,
            'user_id' => auth()->user()->id,
            'score' => $score
        ]);

        // $test_result->answers()->createMany($answer);

        return redirect()->route('list.exam')->with(['success' => 'Score anda : ' . $score]);
        
    }
}
