<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class HelperController extends Controller
{
    public function image($path)
    {
        try {
            if (Storage::exists($path = Crypt::decrypt($path, false))) {
                return response()->file(Storage::path($path));
            }
        } catch (\Throwable $th) {
            abort(404);
        }

        abort(404);
    }
}
