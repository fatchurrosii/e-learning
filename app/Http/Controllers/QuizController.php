<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quiz = Quiz::paginate();
        return view('pages.dashboard.quiz.subject.index', compact('quiz'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);

        $quiz = Quiz::create([
            'title' => $request->title
        ]);

        if ($quiz) {
            return redirect()->route('subject-quiz.index')->with(['success' => 'Data berhasil disimpan!']);
        } else {
            return redirect()->route('subject-quiz.index')->with(['error' => 'Data gagal disimpan!']);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = Quiz::all();

        return view('pages.dashboard.quiz.subject.index',compact($quiz));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quiz = Quiz::find($id);
        $quiz->title = $request->title;
        $quiz->save();
        if ($quiz) {
            return redirect()->route('subject-quiz.index')->with(['success' => 'Data berhasil diedit!']);
        } else {
            return redirect()->route('subject-quiz.index')->with(['error' => 'Data gagal diedit!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::find($id);

        $quiz->delete();

        return redirect()->route('subject-quiz.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
