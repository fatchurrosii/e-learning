<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Kelas;
use App\Models\Mentor;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $siswa = Siswa::with('user')->paginate(5);
        $siswaCount = User::where('role_id', 3)->where('status', 'Active')->count();
        $kelas = Kelas::all()->count();
        $mentor = Mentor::all()->count();
        return view('pages.dashboard.index', compact(['siswa', 'kelas', 'mentor', 'siswaCount']));
    }
}
