<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MentorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       $result = [
        'id' => $this->id,
        'user_id' => $this->user_id,
        'alamat' => $this->alamat,
        'no_hp' => $this->no_hp,
        'photo' => $this->photo,
        'user' => new UserResource($this->whenLoaded('user'))
       ];
       return $result;
    }
}
