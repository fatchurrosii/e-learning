<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SiswaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       $result = [

        'id' => $this->id,
        'user_id' => $this->user_id,
        'id_siswa_categories' => $this->id_siswa_categories,
        'alamat' => $this->alamat,
        'no_hp' => $this->no_hp,
        'photo' => $this->photo,
        'kelas' => $this->kelas,
        'user' => new UserResource($this->whenLoaded('user'))

       ];
       return $result;
    }
}
