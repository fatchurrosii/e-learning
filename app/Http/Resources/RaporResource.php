<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RaporResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = [

            'id' =>  $this->id,
            'siswa_id' =>  $this->siswa_id,
            'mentor_id' =>  $this->mentor_id,
            'speaking' => $this->speaking,
            'reading' => $this->reading,
            'listening' => $this->listening,
            'progress_belajar' => $this->progress_belajar,
            'created_at' => $this->created_at,
            'siswa' => new SiswaResource($this->whenLoaded('siswa')),
            'mentor' => new MentorResource($this->whenLoaded('mentor'))

        ];
        return $result;
    }
}
