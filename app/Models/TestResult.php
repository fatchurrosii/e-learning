<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestResult extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'exam_id',
        'user_id',
        'score'
    ];
    public function answers(){
        return $this->hasMany(ExamAnswer::class);
    }
    public function exam(){
        return $this->belongsTo(Exam::class, 'exam_id');
    }
}
