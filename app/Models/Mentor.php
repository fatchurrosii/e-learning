<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mentor extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mentor';
    protected $fillable = [
        'user_id',
        'alamat',
        'no_hp',
        'photo'
    ];

    public function rapor()
    {
        return $this->hasMany(Rapor::class, 'mentor_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
