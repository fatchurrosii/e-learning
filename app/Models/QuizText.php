<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizText extends Model
{
    use HasFactory;
    protected $guarded = [
        'id'
    ];

    public function question(){
        return $this->hasOne(Question::class, 'quiz_text_id','id');
    }
}
