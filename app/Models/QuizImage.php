<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizImage extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function question(){
        return $this->hasMany(Question::class, 'quiz_images_id','id');
    }
}