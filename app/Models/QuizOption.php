<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizOption extends Model
{
    use HasFactory, SoftDeletes;

    protected $guard = ['id'];
    protected $fillable = [
        'question_id',
        'option_text',
        'correct',
        'created_by'
    ];

    public function question(){
        $this->belongsTo(Question::class);
    }
}
