<?php

namespace App\Models;

use App\Http\Controllers\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;
    protected $guarded = [
        'id'
    ];
    protected $table = 'quiz';

    public function questions(){
        return $this->hasMany(Question::class, 'quiz_id', 'id');
    }
}
