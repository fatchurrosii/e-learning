<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rapor extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [

        'siswa_id',
        'mentor_id',
        'speaking',
        'reading',
        'listening',
        'progress_belajar',
        'speaking_note',
        'reading_note',
        'listening_note',
    ];

    public function siswa()
    {
        return $this->belongsTo(Siswa::class, 'siswa_id');
    }

    public function mentor()
    {
        return $this->belongsTo(Mentor::class, 'mentor_id');
    }

}
