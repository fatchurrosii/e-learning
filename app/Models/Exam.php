<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
     
    protected $fillable = [
        'name',
        'image',
        'status'
    ];

    public function exam_question(){
        return $this->hasMany(ExamQuestion::class, 'exam_id', 'id');
    }
    public function test_result(){
        return $this->hasMany(TestResult::class,'exam_id', 'id');
    }

    public function exam_students(){
        return $this->hasMany(ExamStudent::class, 'exam_id', 'id');
    }
}
