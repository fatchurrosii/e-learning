<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizSound extends Model
{
    use HasFactory;
    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'id',
        'title',
        'path',
        'caption',
        'created_by'
    ];

    public function question(){
        return $this->hasMany(Question::class, 'quiz_sounds_id','id');
    }
}
