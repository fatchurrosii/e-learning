<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    use HasFactory;
    protected $table = 'answer_exams';
    protected $fillable = ['test_result_id', 'question_id', 'quiz_option_id', 'correct'];
}

