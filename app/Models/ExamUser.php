<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamUser extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $fillable = ['exam_id', 'siswa_id'];
    protected $table =  'exam_users';

    public function exams(){
        return $this->belongsToMany(Exam::class);
    }
}
