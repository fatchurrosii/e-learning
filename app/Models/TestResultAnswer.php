<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestResultAnswer extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['test_result_id', 'question_id', 'quiz_option_id', 'correct'];
}
