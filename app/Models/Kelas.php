<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelas extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $fillable = [
        'user_id',
        'id_kategori_kelas',
        'title',
        'embed_video',
        'deksripsi',
        'image'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kategori_kelas()
    {
        return $this->belongsTo(KategoriKelas::class, 'id_kategori_kelas');
    }
    public function kelas_students(){
        return $this->hasMany(KelasStudent::class, 'kelas_id', 'id');
    }
    public function kelas_student(){
        return $this->hasOne(KelasStudent::class, 'kelas_id', 'id');
    }
}
