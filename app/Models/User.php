<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $date = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'role_id'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */


    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    public function student(){
        return $this->hasOne(Siswa::class, 'user_id','id');
    }
    public function siswa()
    {
        return $this->hasMany(Siswa::class, 'user_id', 'id');
    }
    public function mentors(){
        return $this->hasOne(Mentor::class, 'user_id', 'id');
    }
    public function mentor()
    {
        return $this->hasMany(Mentor::class, 'user_id', 'id');
    }
    public function kelas()
    {
        return $this->hasMany(Kelas::class, 'user_id', 'id');
    }
    public function rapor(){
        return $this->hasMany(Rapor::class, 'siswa_id', 'id');
    }
    public function test_result(){
        return $this->hasMany(TestResult::class,'user_id', 'id');
    }
}
