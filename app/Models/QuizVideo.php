<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuizVideo extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function question(){
        return $this->hasMany(Question::class, 'quiz_videos_id', 'id');
    }
}
