<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $guarded =[
        'id'
    ];

    public function quiz(){
        return $this->belongsTo(Quiz::class);
    }

    public function exam(){

        return $this->belongsTo(Exam::class);
    }

    public function question_options(){
        return $this->hasMany(QuizOption::class, 'question_id', 'id');
    }

    public function exam_question(){
        return $this->hasMany(ExamQuestion::class, 'question_id', 'id');
    }

    public function quiz_images(){
        return $this->belongsTo(QuizImage::class, 'quiz_images_id','id' );
    }
    public function quiz_videos(){
        return $this->belongsTo(QuizVideo::class, 'quiz_videos_id','id' );
    }
    public function quiz_texts(){
        return $this->belongsTo(QuizText::class, 'quiz_text_id','id' );
    }
    public function quiz_sounds(){
        return $this->belongsTo(QuizSound::class, 'quiz_sounds_id','id' );
    }
}
