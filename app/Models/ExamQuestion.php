<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class ExamQuestion extends Model
{
    use HasFactory, SoftDeletes, Userstamps;

    protected $guarded = ['id'];
    protected $fillable = [
        'exam_id',
        'question_id',
    ];

    public function exam(){
        return $this->belongsTo(Exam::class, 'exam_id','id');
    }
    public function question(){
        return $this->belongsTo(Question::class, 'question_id','id');
    }
}
