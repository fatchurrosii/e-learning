<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siswa extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'siswa';

    protected $fillable = [
        'mentor_id',
        'user_id',
        'alamat',
        'no_hp',
        'photo',
        'kelas'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kategori_siswa()
    {
        return $this->belongsTo(KategoriSiswa::class, 'id_siswa_categories');
    }

    public function rapor()
    {
        return $this->hasMany(Rapor::class, 'siswa_id', 'id');
    }
    public function siswa_kelas(){
        return $this->hasMany(KelasStudent::class, 'siswa_id', 'id');
    }
    public function exam_students(){
        return $this->hasMany(ExamStudent::class,'siswa_id', 'id');
    }
}
