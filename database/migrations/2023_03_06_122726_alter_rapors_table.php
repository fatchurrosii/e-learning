<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRaporsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rapors', function (Blueprint $table) {
            $table->text('reading_note')->nullable();
            $table->text('speaking_note')->nullable();
            $table->text('listening_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rapors', function (Blueprint $table) {
            //
        });
    }
}
