<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestResultAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_result_answers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('test_result_id')->constrained('test_results', 'id')->cascadeOnDelete();
            $table->foreignId('question_id')->constrained('questions', 'id')->cascadeOnDelete();
            $table->foreignId('quiz_option_id')->constrained('quiz_options', 'id')->cascadeOnDelete();
            $table->integer('correct');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_result_answers');
    }
}
