<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterQuizTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_text', function (Blueprint $table) {
            $table->rename('quiz_texts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_text', function (Blueprint $table) {
            Schema::drop('quiz_text');

            Schema::dropIfExists('quiz_texts');
        });
    }
}
