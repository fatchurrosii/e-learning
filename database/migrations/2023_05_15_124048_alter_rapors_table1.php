<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRaporsTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rapors', function (Blueprint $table) {
            $table->dropColumn('speaking_note');
            $table->dropColumn('listening_note');
            $table->dropColumn('reading_note');
            $table->dropColumn('speaking');
            $table->dropColumn('listening');
            $table->dropColumn('reading');
            $table->integer('rapor_score'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rapors', function (Blueprint $table) {
            
        });
    }
}
