<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('quiz_id')->index('fk_questions_to_quiz');
            $table->string('name');
            $table->unsignedBigInteger('quiz_images_id')->nullable();
            $table->unsignedBigInteger('quiz_sounds_id')->nullable();
            $table->unsignedBigInteger('quiz_text_id')->nullable();
            $table->foreign('quiz_images_id')->references('id')->on('quiz_images')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('quiz_sounds_id')->references('id')->on('quiz_sounds')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('quiz_text_id')->references('id')->on('quiz_text')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
