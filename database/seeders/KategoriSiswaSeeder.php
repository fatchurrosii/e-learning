<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriSiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswa_categories')->insert([
            'name' => 'Junior',
        ]);

        DB::table('siswa_categories')->insert([
            'name' => 'Middle',
        ]);
    }
}
