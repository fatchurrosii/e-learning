<?php

namespace Database\Seeders;

use App\Models\KategoriKelas;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            KategoriSiswaSeeder::class,
            KategoriKelasSeeder::class,
            UserSeeder::class
        ]);
    }
}
