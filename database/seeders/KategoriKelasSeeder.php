<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas_categories')->insert([
            [
                'name' => 'Speaking'
            ],
            [
                'name' => 'Reading'
            ],
            [
                'name' => 'Writing'
            ],
        ]);
    }
}
