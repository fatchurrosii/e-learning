<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KategoriSiswaController;
use App\Http\Controllers\Landing\HalamanKelasController;
use App\Http\Controllers\Landing\ProfileController;
use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
Route::get('/edit', [ProfileController::class, 'edit'])->name('edit.profile');
Route::put('/update', [ProfileController::class, 'update'])->name('update.profile');
Route::get('/header', [ProfileController::class, 'header'])->name('header');
Route::get('/cetak/{raporId}', [ProfileController::class, 'cetak'])->name('print');
Route::get('/cetak-siswa', [ProfileController::class, 'cetakSiswa'])->name('siswa-print');
// Authentication
Route::get('login', [AuthController::class, 'login'])->name('login');
Route::get('register', [AuthController::class, 'register'])->name('register');
Route::post('register', [AuthController::class, 'registerUser'])->name('registerUser');
Route::post('login', [AuthController::class, 'authenticate']);
Route::post('logout', [AuthController::class, 'logout'])->name('logout');

// Route::middleware(['auth'], function(){
    Route::post('/reset-password', [ProfileController::class, 'resetPassword'])->name('reset-password');
    // });
    // route::get('kelas', [HalamanKelasController::class, 'getKelas'])->name('kelas');
    
    Route::group(['prefix' => 'kelas', 'middleware' => ['auth']], function () {
        Route::get('/', [HalamanKelasController::class, 'index'])->name('list.kelas');
        Route::get('/{kelasId}', [HalamanKelasController::class, 'show'])->name('detail.kelas');
        Route::put('/{kelasId}', [HalamanKelasController::class, 'changeStatus'])->name('changeStatus');
});

Route::group(['prefix' => 'exam', 'middleware' => ['auth']], function(){
    Route::get('/', [HalamanQuizController::class, 'index'])->name('list.exam');
    Route::get('/{examId}', [HalamanQuizController::class, 'show'])->name('detail.exam');
    Route::post('/{examId}', [HalamanQuizController::class, 'test'])->name('test');
});
Route::group([
    'middleware' => ['auth']
], function () {
    Route::group(['prefix' => 'dashboard', 'middleware' => ['role:Admin,Mentor']], function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
        Route::resource('role', RoleController::class);
        Route::resource('user', UserController::class);
        Route::get('/user/{id}/{status}', [UserController::class, 'changeStatus'])->name('change-status');
        Route::post('/user/{id}/reset', [UserController::class, 'reset'])->name('reset');
        Route::resource('kategori-siswa', KategoriSiswaController::class);
        Route::resource('siswa', SiswaController::class);
        Route::resource('mentor', MentorController::class);
        Route::resource('class', KelasController::class);
        Route::get('/class/{kelasId}/assign', [KelasController::class, 'assign_kelas'])->name('assign_kelas');
        Route::post('/class/{kelasId}/assign', [KelasController::class, 'assign'])->name('assign');
        Route::resource('rapor', RaporController::class);
        Route::get('/cetak/{id}', [RaporController::class, 'cetak'])->name('cetak');
        
        Route::group(['prefix' => 'quiz'], function(){
            
            Route::resource('quiz-image', QuizImageController::class);
            Route::resource('quiz-sound', QuizSoundController::class);
            Route::resource('quiz-text', QuizTextController::class);
            Route::resource('quiz-video', QuizVideoController::class);
            
            Route::resource('question', QuestionController::class);
            Route::resource('exam', ExamController::class);
            Route::get('/exam/{examId}/assign', [ExamController::class, 'assign_exam'])->name('assign.exam');
            Route::post('/exam/{examId}/student', [ExamController::class, 'student'])->name('store_assign');
        });
    });
});
Route::get('/{path}', [HelperController::class, 'image'])->name('files.image');
Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('files');
